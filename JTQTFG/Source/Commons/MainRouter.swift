//
//  MainRouter.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit

protocol MainRouterInterface {
    func presentTabBarController()
}

class MainRouter {

    let window: UIWindow
    var tabBarRouter: TabBarRouterProtocol?

    init(mainWindow: UIWindow) {
        self.window = mainWindow
    }

    var rootViewController: UIViewController {
        guard let rootViewController = window.rootViewController else {
            fatalError("There is no rootViewController installed on the window")
        }
        return rootViewController
    }
}

extension MainRouter {
    private func push(viewController: UIViewController, animated: Bool) {
        if rootViewController.navigationController == nil {
            (rootViewController as? UINavigationController)?.pushViewController(viewController, animated: animated)
        } else {
            rootViewController.navigationController?.pushViewController(viewController, animated: animated)
        }
    }
}

extension MainRouter: MainRouterInterface {
    func presentTabBarController() {
        var nestedRouters = [NestedTabBarViewRouterProtocol]()

        let workRouter = WorkRouter(mainRouter: self)
        let tasksRouter = TasksRouter(mainRouter: self)
        let exceptionsRouter = ExceptionsRouter(mainRouter: self)
        let settingsRouter = SettingsRouter(mainRouter: self)

        nestedRouters = [workRouter, tasksRouter, exceptionsRouter, settingsRouter]

        self.tabBarRouter = TabBarRouter.installIntoWindow(mainRouter: self,
                                                           window: window,
                                                           nestedRouters: nestedRouters)
    }
}
