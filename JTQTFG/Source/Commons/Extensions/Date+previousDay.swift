//
//  Date+previousDay.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 19/05/2020.
//  Copyright © 2020 José Tordesillas Quintana. All rights reserved.
//

import Foundation

extension Date {
    var previousDay: Date {
        return self.addingTimeInterval(-(60*60*24))
    }
}
