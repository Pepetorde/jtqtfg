//
//  UIScrollView+scrollTo.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 07/05/2020.
//  Copyright © 2020 José Tordesillas Quintana. All rights reserved.
//

import UIKit

extension UIScrollView {
    func scrollTo(horizontalPage: Int, animated: Bool) {
        var frame: CGRect = self.frame
        frame.origin.x = frame.size.width * CGFloat(horizontalPage)
        frame.origin.y = 0
        self.scrollRectToVisible(frame, animated: animated)
    }
}
