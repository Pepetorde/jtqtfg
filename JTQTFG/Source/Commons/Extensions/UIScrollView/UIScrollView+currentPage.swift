//
//  UIScrollView+currentPage.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 07/05/2020.
//  Copyright © 2020 José Tordesillas Quintana. All rights reserved.
//

import UIKit

extension UIScrollView {
   var currentPageIndex: Int {
      return Int((self.contentOffset.x + (0.5*self.frame.size.width))/self.frame.width)
   }
}
