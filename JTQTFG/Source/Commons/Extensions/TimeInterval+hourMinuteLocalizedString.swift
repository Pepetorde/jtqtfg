//
//  TimeInterval+hourMinuteLocalizedString.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 15/03/2020.
//  Copyright © 2020 José Tordesillas Quintana. All rights reserved.
//

import Foundation

extension TimeInterval {
    func hourMinuteLocalizedString(style: DateComponentsFormatter.UnitsStyle) -> String {
        let timeFormatter = DateComponentsFormatter()
        timeFormatter.allowedUnits = [.hour, .minute]
        timeFormatter.unitsStyle = style

        return timeFormatter.string(from: self)!
    }
}
