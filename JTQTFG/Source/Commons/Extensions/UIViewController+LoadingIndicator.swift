//
//  UIViewController+LoadingIndicator.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 22/06/2020.
//  Copyright © 2020 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func showLoadingIndicator() {
        if self.parent != nil {
            let overlay = createOverlayView()
            let loadingIndicator = createLoadingIndicator(onView: overlay)
            let squareView = createSquareView()

            self.view.addSubview(overlay)
            overlay.addSubview(squareView)
            overlay.addSubview(loadingIndicator)
            overlay.bringSubviewToFront(loadingIndicator)

            UIView.animate(withDuration: 0.2, animations: {
                loadingIndicator.alpha = 1.0
            })
        }
    }

    private func createOverlayView() -> UIView {
        let overlay = UIView(frame: parent!.view.bounds)
        overlay.tag = 99
        overlay.center = parent!.view.center
        overlay.backgroundColor = UIColor(white: 1, alpha: 0.6)
        return overlay
    }

    private func createLoadingIndicator(onView overlay: UIView) -> UIView {
        let loadingIndicator = UIActivityIndicatorView(style: .whiteLarge)
        loadingIndicator.center = overlay.center
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.startAnimating()
        return loadingIndicator
    }

    private func createSquareView() -> UIView {
        let squareView: UIView = UIView()
        squareView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        squareView.center = parent!.view.center
        squareView.backgroundColor = .darkGray
        squareView.layer.cornerRadius = 10

        return squareView
    }

    func hideLoadingIndicator() {
        self.view.viewWithTag(99)?.removeFromSuperview()
    }
}
