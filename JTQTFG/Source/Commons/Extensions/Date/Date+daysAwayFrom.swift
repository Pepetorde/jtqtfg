//
//  Date+daysAwayFrom.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 01/06/2020.
//  Copyright © 2020 José Tordesillas Quintana. All rights reserved.
//

import Foundation

extension Date {
    func daysAwayFrom(_ comparedDate: Date) -> Int {
        var firstDate: Date
        var secondDate: Date

        if comparedDate > self {
            firstDate = self.startOfDay
            secondDate = comparedDate.startOfDay
        } else {
            secondDate = self.startOfDay
            firstDate = comparedDate.startOfDay
        }

        let components = Calendar.current.dateComponents([.day], from: firstDate, to: secondDate)
        return components.day!
    }
}
