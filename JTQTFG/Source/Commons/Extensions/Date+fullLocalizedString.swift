//
//  Date+fullLocalizedString.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 15/03/2020.
//  Copyright © 2020 José Tordesillas Quintana. All rights reserved.
//

import Foundation

extension Date {
    func fullLocalizedString() -> String {
        return DateFormatter.localizedString(
        from: self,
        dateStyle: .full,
        timeStyle: .none)
    }

    func longLocalizedString() -> String {
        return DateFormatter.localizedString(
        from: self,
        dateStyle: .long,
        timeStyle: .none)
    }

    func monthLocalizedString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        return dateFormatter.string(from: self)
    }
}
