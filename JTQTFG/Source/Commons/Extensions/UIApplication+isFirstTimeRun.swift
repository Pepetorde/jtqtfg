//
//  UIApplication+isFirstTimeRun.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 04/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    func isFirstLaunch() -> Bool {
        let defaults = UserDefaults.standard
        if defaults.string(forKey: "isAppAlreadyLaunchedOnce") != nil {
            return false
        } else {
            defaults.set(true, forKey: "isAppAlreadyLaunchedOnce")
            return true
        }
    }
}
