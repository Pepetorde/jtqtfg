//
//  UILabel+softShadow.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 26/05/2020.
//  Copyright © 2020 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    func softShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.6
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
    }

    func noSoftShadow() {
        self.layer.masksToBounds = true
        self.layer.shadowRadius = 0
        self.layer.shadowOpacity = 0
    }
}
