//
//  Sequence+Sum.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 13/01/2020.
//  Copyright © 2020 José Tordesillas Quintana. All rights reserved.
//

import Foundation

extension Sequence where Element: AdditiveArithmetic {
    func sum() -> Element {
        return reduce(.zero, +)
    }
}
