//
//  Date+today.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 05/03/2020.
//  Copyright © 2020 José Tordesillas Quintana. All rights reserved.
//

import Foundation

extension Date {
    public static func today() -> Date {
        return Date().startOfDay
    }
}
