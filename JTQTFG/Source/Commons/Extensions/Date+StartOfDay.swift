//
//  Date+StartOfDay.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 26/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation

extension Date {
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
}
