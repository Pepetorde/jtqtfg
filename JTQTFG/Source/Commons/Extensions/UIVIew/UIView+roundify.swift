//
//  UIView+roundify.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 25/05/2020.
//  Copyright © 2020 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func roundify() {
        let radius = self.bounds.width / 2
        self.layer.cornerRadius = radius
    }
}
