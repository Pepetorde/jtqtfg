//
//  StoredException+CoreDataProperties.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 07/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//
//

import Foundation
import CoreData

extension StoredException {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StoredException> {
        return NSFetchRequest<StoredException>(entityName: "StoredException")
    }

    @NSManaged public var date: NSDate?
    @NSManaged public var workTime: Double

}
