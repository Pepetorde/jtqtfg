//
//  StoredSchedule+CoreDataProperties.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 04/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//
//

import Foundation
import CoreData

extension StoredSchedule {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StoredSchedule> {
        return NSFetchRequest<StoredSchedule>(entityName: "StoredSchedule")
    }

    @NSManaged public var friday: Int64
    @NSManaged public var monday: Int64
    @NSManaged public var saturday: Int64
    @NSManaged public var sunday: Int64
    @NSManaged public var thursday: Int64
    @NSManaged public var tuesday: Int64
    @NSManaged public var wednesday: Int64

}
