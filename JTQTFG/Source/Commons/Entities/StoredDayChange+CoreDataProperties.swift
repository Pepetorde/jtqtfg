//
//  StoredDayChange+CoreDataProperties.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 06/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//
//

import Foundation
import CoreData

extension StoredDayChange {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StoredDayChange> {
        return NSFetchRequest<StoredDayChange>(entityName: "StoredDayChange")
    }

    @NSManaged public var hour: Int16
    @NSManaged public var minute: Int16

}
