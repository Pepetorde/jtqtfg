//
//  StoredException+CoreDataClass.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 07/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//
//

import Foundation
import CoreData

@objc(StoredException)
public class StoredException: NSManagedObject {

}
