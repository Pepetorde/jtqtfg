//
//  StoredWorkFragment+CoreDataProperties.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 10/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//
//

import Foundation
import CoreData

extension StoredWorkFragment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StoredWorkFragment> {
        return NSFetchRequest<StoredWorkFragment>(entityName: "StoredWorkFragment")
    }

    @NSManaged public var workTime: Double
    @NSManaged public var done: Bool
    @NSManaged public var task: StoredTask?

}
