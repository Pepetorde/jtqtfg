//
//  StoredTask+CoreDataProperties.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 04/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//
//

import Foundation
import CoreData

extension StoredTask {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StoredTask> {
        return NSFetchRequest<StoredTask>(entityName: "StoredTask")
    }
}

extension StoredTask: Equatable {
    static func == (lhs: StoredTask, rhs: StoredTask) -> Bool {
        if lhs.name == rhs.name && lhs.startDate == rhs.startDate && lhs.deadline == rhs.deadline {
            return true
        } else {
            return false
        }
    }
}
