//
//  TabBarRouter.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import UIKit

class TabBarRouter: TabBarRouterProtocol {

    let mainRouter: MainRouterInterface?

    init(mainRouter: MainRouterInterface? = nil) {
        self.mainRouter = mainRouter
    }

    static func installIntoWindow(mainRouter: MainRouter,
                                  window: UIWindow,
                                  nestedRouters: [NestedTabBarViewRouterProtocol]) -> TabBarRouterProtocol {

        let view = TabBarViewController()
        let router = TabBarRouter(mainRouter: mainRouter)
        let presenter = TabBarPresenter(view: view, router: router)

        view.presenter = presenter

        let tabBarController = view
        tabBarController.viewControllers = createModules(from: nestedRouters)
        tabBarController.selectedIndex = 0
        window.rootViewController = tabBarController

        return router
    }

    static private func createModules(from routers: [NestedTabBarViewRouterProtocol]) -> [UIViewController] {
        var viewControllers = [UIViewController]()

        for router in routers {
            let navigationController = setNavigationController(from: router)
            viewControllers.append(navigationController)
        }

        return viewControllers
    }

    static private func setNavigationController(from router: NestedTabBarViewRouterProtocol) -> UINavigationController {
        let viewController = router.getAnidatedTabBarModule()
        let navigationController = UINavigationController(rootViewController: viewController)

        navigationController.tabBarItem = setTabBarItem(from: router)
        navigationController.setNavigationBarHidden(false, animated: false)

        return navigationController
    }

    static private func setTabBarItem(from router: NestedTabBarViewRouterProtocol) -> UITabBarItem {
        let tabBarItem = UITabBarItem()
        tabBarItem.image = router.tabIcon
        tabBarItem.title = router.tabTitle

        return tabBarItem
    }
}
