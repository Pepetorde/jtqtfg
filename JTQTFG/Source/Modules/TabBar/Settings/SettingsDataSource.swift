//
//  SettingsDataSource.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 04/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class SettingsDataSource: SettingsDataSourceProtocol {

    var interactor: SettingsDataSourceOutputProtocol?

    weak var appDelegate: AppDelegate?
    let managedContext: NSManagedObjectContext?

    init() {
        self.appDelegate = UIApplication.shared.delegate as? AppDelegate
        self.managedContext = appDelegate?.persistentContainer.viewContext
    }
}

extension SettingsDataSource {
    func retrieveSchedule() {
        let fetchRequest: NSFetchRequest<StoredSchedule> = StoredSchedule.fetchRequest()

        do {
            let results = try managedContext?.fetch(fetchRequest)
            interactor?.retrieveScheduleDidSuccess(retrievedSchedule: results![0])
        } catch let error as NSError {
            print("No ha sido posible cargar \(error), \(error.userInfo)")
            interactor?.retrieveScheduleDidFail()
        }
    }

    func updateSchedule(newSchedule: ScheduleInput) {
        let fetchRequest: NSFetchRequest<StoredSchedule> = StoredSchedule.fetchRequest()

        do {
            let results = try managedContext?.fetch(fetchRequest)
            let fetchedSchedule = results![0]

            fetchedSchedule.sundayTime = newSchedule.workTimeStartingOnSunday[0]
            fetchedSchedule.mondayTime = newSchedule.workTimeStartingOnSunday[1]
            fetchedSchedule.tuesdayTime = newSchedule.workTimeStartingOnSunday[2]
            fetchedSchedule.wednesdayTime = newSchedule.workTimeStartingOnSunday[3]
            fetchedSchedule.thursdayTime = newSchedule.workTimeStartingOnSunday[4]
            fetchedSchedule.fridayTime = newSchedule.workTimeStartingOnSunday[5]
            fetchedSchedule.saturdayTime = newSchedule.workTimeStartingOnSunday[6]

            fetchedSchedule.sundayOptional = newSchedule.optionalStartingOnSunday[0]
            fetchedSchedule.mondayOptional = newSchedule.optionalStartingOnSunday[1]
            fetchedSchedule.tuesdayOptional = newSchedule.optionalStartingOnSunday[2]
            fetchedSchedule.wednesdayOptional = newSchedule.optionalStartingOnSunday[3]
            fetchedSchedule.thursdayOptional = newSchedule.optionalStartingOnSunday[4]
            fetchedSchedule.fridayOptional = newSchedule.optionalStartingOnSunday[5]
            fetchedSchedule.saturdayOptional = newSchedule.optionalStartingOnSunday[6]

            do {
                try managedContext?.save()
                UserDefaults.standard.set(true, forKey: "scenarioHasChanged")
                self.interactor?.updateScheduleDidSuccess()
            } catch let error as NSError {
                print("No ha sido posible actualizar \(error), \(error.userInfo)")
                interactor?.updateScheduleDidFail()
            }
        } catch let error as NSError {
            print("No ha sido posible actualizar \(error), \(error.userInfo)")
            interactor?.updateScheduleDidFail()
        }
    }

    func retrieveDayChange() {
        let fetchRequest: NSFetchRequest<StoredDayChange> = StoredDayChange.fetchRequest()

        do {
            let results = try managedContext?.fetch(fetchRequest)
            interactor?.retrieveDayChangeDidSuccess(retrievedDayChange: results![0])
        } catch let error as NSError {
            print("No ha sido posible cargar \(error), \(error.userInfo)")
            interactor?.retrieveDayChangeDidFail()
        }
    }

    func updateDayChange(newDayChange: DayChange) {
        let fetchRequest: NSFetchRequest<StoredDayChange> = StoredDayChange.fetchRequest()

        do {
            let results = try managedContext?.fetch(fetchRequest)
            let fetchedDayChange = results![0]

            fetchedDayChange.hour = Int16(newDayChange.hour)
            fetchedDayChange.minute = Int16(newDayChange.minute)

            do {
                try managedContext?.save()
                UserDefaults.standard.set(true, forKey: "scenarioHasChanged")
                UserDefaults.standard.set(true, forKey: "dayChangeHasChanged")
                self.interactor?.updateDayChangeDidSuccess()
            } catch let error as NSError {
                print("No ha sido posible actualizar \(error), \(error.userInfo)")
                interactor?.updateDayChangeDidFail()
            }
        } catch let error as NSError {
            print("No ha sido posible actualizar \(error), \(error.userInfo)")
            interactor?.updateDayChangeDidFail()
        }
    }
}

struct ScheduleInput {
    var workTimeStartingOnSunday: [Double]
    var optionalStartingOnSunday: [Bool]

    init() {
        workTimeStartingOnSunday = Array(repeating: 0, count: 7)
        optionalStartingOnSunday = Array(repeating: false, count: 7)
    }

    init(from schedule: StoredSchedule) {
        workTimeStartingOnSunday = []
        let scheduleTimeArray = [schedule.sundayTime, schedule.mondayTime, schedule.tuesdayTime, schedule.wednesdayTime,
                             schedule.thursdayTime, schedule.fridayTime, schedule.saturdayTime]

        for index in 0..<scheduleTimeArray.count {
            workTimeStartingOnSunday.append(scheduleTimeArray[index])
        }

        optionalStartingOnSunday = []
        let scheduleOptionalArray = [schedule.sundayOptional, schedule.mondayOptional,
                                     schedule.tuesdayOptional, schedule.wednesdayOptional,
                             schedule.thursdayOptional, schedule.fridayOptional,
                             schedule.saturdayOptional]

        for index in 0..<scheduleOptionalArray.count {
            optionalStartingOnSunday.append(scheduleOptionalArray[index])
        }
    }
}

struct DayChange {
    var hour: Int
    var minute: Int

    init() {
        self.hour = 0
        self.minute = 0
    }

    init(from dayChange: StoredDayChange) {
        self.hour = Int(dayChange.hour)
        self.minute = Int(dayChange.minute)
    }
}
