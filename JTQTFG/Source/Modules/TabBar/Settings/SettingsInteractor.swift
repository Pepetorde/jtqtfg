//
//  SettingsInteractor.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 04/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import CoreData

class SettingsInteractor: SettingsInteractorProtocol {

    var presenter: SettingsInteractorOutputProtocol?
    private let dataSource: SettingsDataSourceProtocol

    init(dataSource: SettingsDataSourceProtocol) {
        self.dataSource = dataSource
    }
}

extension SettingsInteractor {
    func retrieveSchedule() {
        dataSource.retrieveSchedule()
    }

    func updateSchedule(newSchedule: ScheduleInput) {
        dataSource.updateSchedule(newSchedule: newSchedule)
    }

    func retrieveDayChange() {
        dataSource.retrieveDayChange()
    }

    func updateDayChange(newDayChange: DayChange) {
        dataSource.updateDayChange(newDayChange: newDayChange)
    }

}

extension SettingsInteractor: SettingsDataSourceOutputProtocol {

    func updateScheduleDidSuccess() {
        presenter?.updateScheduleDidSuccess()
    }

    func updateScheduleDidFail() {
        presenter?.updateScheduleDidFail()
    }

    func retrieveScheduleDidSuccess(retrievedSchedule: StoredSchedule) {
        presenter?.retrieveScheduleDidSuccess(retrievedSchedule: retrievedSchedule)
    }

    func retrieveScheduleDidFail() {
        presenter?.retrieveScheduleDidFail()
    }

    func updateDayChangeDidSuccess() {
        presenter?.updateDayChangeDidSuccess()
    }

    func updateDayChangeDidFail() {
        presenter?.updateDayChangeDidFail()
    }

    func retrieveDayChangeDidSuccess(retrievedDayChange: StoredDayChange) {
        presenter?.retrieveDayChangeDidSuccess(retrievedDayChange: retrievedDayChange)
    }

    func retrieveDayChangeDidFail() {
        presenter?.retrieveDayChangeDidFail()
    }
}
