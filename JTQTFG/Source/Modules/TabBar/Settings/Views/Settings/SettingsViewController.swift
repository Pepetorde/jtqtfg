//
//  SettingsViewController.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import UIKit

class SettingsViewController: ParentViewController {

    @IBOutlet weak var settingsTable: UITableView!

    var dayChange: StoredDayChange?
    var schedule: StoredSchedule?
    @IBOutlet weak var iconsByLabel: UILabel!

    var presenter: SettingsPresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = NSLocalizedString("settings.navTitle", comment: "")
        presenter?.retrieveDayChange()
    }
}

extension SettingsViewController {
    private func configureView() {
        setTextsInLanguage()
        settingsTable.dataSource = self
        settingsTable.delegate = self
    }

    private func setTextsInLanguage() {
        iconsByLabel.text = NSLocalizedString("settings.iconsBy", comment: "")
    }

    @IBAction func icons8LinkDidTouchUpInside(_ sender: Any) {
        presenter?.icons8LinkPressed()
    }
}

extension SettingsViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let scheduleCell = UITableViewCell(style: .default, reuseIdentifier: nil)
            scheduleCell.textLabel?.text = NSLocalizedString("settings.schedule", comment: "")
            scheduleCell.accessoryType = .disclosureIndicator
            return scheduleCell
        }

        if indexPath.row == 1 {
            var auxiliaryDate = Date()
            auxiliaryDate = Calendar.current.date(bySetting: .minute, value: Int(dayChange!.minute), of: auxiliaryDate)!
            auxiliaryDate = Calendar.current.date(bySetting: .hour, value: Int(dayChange!.hour), of: auxiliaryDate)!

            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .none
            dateFormatter.timeStyle = .short

            let formatedTime = dateFormatter.string(from: auxiliaryDate)

            let dayChangeCell = UITableViewCell(style: .value1, reuseIdentifier: nil)
            dayChangeCell.textLabel?.text = NSLocalizedString("settings.dayChange", comment: "")
            dayChangeCell.detailTextLabel?.text = formatedTime
            dayChangeCell.accessoryType = .disclosureIndicator
            return dayChangeCell
        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            presenter?.scheduleButtonSelected()
        default:
            presenter?.dayChangeButtonSelected()
        }
    }
}

extension SettingsViewController: SettingsViewControllerProtocol {

    func updateDayChangeRow(with dayChange: StoredDayChange) {
        self.dayChange = dayChange
        settingsTable.reloadData()
    }
}
