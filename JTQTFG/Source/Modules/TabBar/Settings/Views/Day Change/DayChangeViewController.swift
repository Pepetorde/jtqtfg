//
//  DayChangeViewController.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 06/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit

class DayChangeViewController: ParentViewController {

    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var dayChangeLabel: UILabel!
    @IBOutlet weak var selectedDayChangeLabel: UILabel!
    @IBOutlet weak var timePicker: UIDatePicker!

    var presenter: SettingsPresenterProtocol?

    var newDayChange = DayChange()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = NSLocalizedString("dayChange.navTitle", comment: "")
        tabBarController?.tabBar.isHidden = true
        presenter?.retrieveDayChange()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
        presenter?.updateDayChange(newDayChange: self.newDayChange)
    }
}

extension DayChangeViewController {

    private func configureView() {
        setTextsInLanguage()
    }

    private func setTextsInLanguage() {
        infoLabel.text = NSLocalizedString("dayChange.info", comment: "")
        dayChangeLabel.text = NSLocalizedString("dayChange.dayChangeLabel", comment: "")
    }
}

extension DayChangeViewController {

}

extension DayChangeViewController {
    @IBAction func datePickerValueDidChange(_ sender: Any) {
        let localizedHour = DateFormatter.localizedString(
            from: timePicker.date,
            dateStyle: .none,
            timeStyle: .short)
        self.selectedDayChangeLabel.text = localizedHour

        newDayChange.hour = Calendar.current.component(.hour, from: timePicker.date)
        newDayChange.minute = Calendar.current.component(.minute, from: timePicker.date)
    }
}

extension DayChangeViewController: DayChangeViewControllerProtocol {
    func updateDayChangeField(with dayChange: StoredDayChange) {
        newDayChange = DayChange(from: dayChange)

        var auxDate = Date()
        auxDate = Calendar.current.date(bySetting: .minute, value: Int(dayChange.minute), of: auxDate)!
        auxDate = Calendar.current.date(bySetting: .hour, value: Int(dayChange.hour), of: auxDate)!

        timePicker.setDate(auxDate, animated: true)

        let localizedHour = DateFormatter.localizedString(
            from: auxDate,
            dateStyle: .none,
            timeStyle: .short)
        self.selectedDayChangeLabel.text = localizedHour
    }
}
