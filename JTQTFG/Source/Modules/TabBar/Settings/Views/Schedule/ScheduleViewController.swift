//
//  ScheduleViewController.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 27/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit

class ScheduleViewController: ParentViewController {

    @IBOutlet weak var scheduleTable: UITableView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var optionalLabel: UILabel!
    @IBOutlet weak var timePicker: UIPickerView!
    @IBOutlet weak var optionalSwitch: UISwitch!

    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    var isBottomScrollViewMarginSetToHigh = false

    var presenter: SettingsPresenterProtocol?

    var newSchedule = ScheduleInput()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        timePicker.delegate = self
        timePicker.dataSource = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = NSLocalizedString("schedule.navTitle", comment: "")
        presenter?.retrieveSchedule()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
        hideAll()
        presenter?.updateSchedule(newSchedule: self.newSchedule)
    }
}

extension ScheduleViewController {
    private func configureView() {
        setTextsInLanguage()
        setTable()
        hideAll()
    }

    private func setTextsInLanguage() {
        infoLabel.text = NSLocalizedString("schedule.info", comment: "")
        optionalLabel.text = NSLocalizedString("schedule.optional", comment: "")
    }

    private func setTable() {
        scheduleTable.delegate = self
        scheduleTable.dataSource = self
        scheduleTable.register(UITableViewCell.self, forCellReuseIdentifier: "scheduleCell")
    }
}

extension ScheduleViewController {
    @IBAction func backgroundButtonDidTouch(_ sender: Any) {
        hideAll()
    }

    @IBAction func optionalSwitchDidChangeValue(_ sender: Any) {
        if let indexPath = scheduleTable.indexPathForSelectedRow {
            let weekDayIndex = (indexPath.row + Calendar.current.firstWeekday - 1) % 7
            newSchedule.optionalStartingOnSunday[weekDayIndex] = optionalSwitch.isOn
            scheduleTable.cellForRow(at: indexPath)?.detailTextLabel?.text = composeCellDetailText(indexPath: indexPath)
        }
    }
}

extension ScheduleViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let scheduleCell = UITableViewCell(style: .value1, reuseIdentifier: "scheduleCell")
        let weekDayIndex = (indexPath.row + Calendar.current.firstWeekday - 1 ) % 7
        let weekDayName = Calendar.current.standaloneWeekdaySymbols[weekDayIndex]
        scheduleCell.textLabel?.text = weekDayName
        scheduleCell.detailTextLabel?.text = composeCellDetailText(indexPath: indexPath)

        return scheduleCell
    }

    private func composeCellDetailText(indexPath: IndexPath) -> String {
        let weekDayIndex = (indexPath.row + Calendar.current.firstWeekday - 1 ) % 7
        var detailTextString = ""

        if newSchedule.workTimeStartingOnSunday[weekDayIndex] == 0 {
            detailTextString = ""
        } else {
            if newSchedule.optionalStartingOnSunday[weekDayIndex] == true {
                let optionalText = NSLocalizedString("schedule.table.optional", comment: "")
                detailTextString.append(optionalText)
            }

            let workTime = newSchedule.workTimeStartingOnSunday[weekDayIndex]
            let localizedTimeInterval = workTime.hourMinuteLocalizedString(style: .full)
            detailTextString.append(localizedTimeInterval)
        }
        return detailTextString
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showTimePicker()
        let weekDayIndex = (indexPath.row + Calendar.current.firstWeekday - 1) % 7
        setTimePickerFrom(timeInterval: newSchedule.workTimeStartingOnSunday[weekDayIndex])
        optionalSwitch.isOn = newSchedule.optionalStartingOnSunday[weekDayIndex]
    }

    private func setTimePickerFrom(timeInterval: TimeInterval) {
        let totalMinutes = Int(timeInterval / 60)
        let minutes = totalMinutes % 60 / 5
        let hours = (totalMinutes - minutes) / 60
        timePicker.selectRow(hours, inComponent: 0, animated: true)
        timePicker.selectRow(minutes, inComponent: 1, animated: true)
    }
}

extension ScheduleViewController {
    private func showTimePicker() {
        tabBarController?.tabBar.isHidden = true
        timePicker.isHidden = false
        optionalLabel.isHidden = false
        optionalSwitch.isHidden = false
        setBottomScrollMarginToHigh()
    }

    private func hideAll() {
        if let row = scheduleTable.indexPathForSelectedRow {
            scheduleTable.deselectRow(at: row, animated: true)
        }
        tabBarController?.tabBar.isHidden = false
        timePicker.isHidden = true
        optionalLabel.isHidden = true
        optionalSwitch.isHidden = true
        setBottomScrollMarginToLow()
    }

    private func setBottomScrollMarginToHigh() {
        if isBottomScrollViewMarginSetToHigh == false {
            bottomConstraint.constant -= timePicker.bounds.height + 90
        }
        isBottomScrollViewMarginSetToHigh = true
    }

    private func setBottomScrollMarginToLow() {
        if isBottomScrollViewMarginSetToHigh {
            bottomConstraint.constant += timePicker.bounds.height + 90
        }
        isBottomScrollViewMarginSetToHigh = false
    }
}

extension ScheduleViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 { //hours
            return 24
        } else { //minutes
            return 60 / 5
        }
    }

    func pickerView(_ pickerView: UIPickerView,
                    attributedTitleForRow row: Int,
                    forComponent component: Int) -> NSAttributedString? {
            if component == 0 { //hours
                return attributedHoursTitleStringFor(row: row)
            } else { // minutes
                return attributedMinutesTitleStringFor(row: row)
            }
    }

    func attributedHoursTitleStringFor(row: Int) -> NSAttributedString {
        let time = TimeInterval(row * 60 * 60)

        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour]
        formatter.unitsStyle = .full

        let title = formatter.string(from: time)!

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .right
        paragraphStyle.tailIndent = -20

        return NSAttributedString(string: title,
                                  attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
    }

    func attributedMinutesTitleStringFor(row: Int) -> NSAttributedString {
        let time = TimeInterval(row * 60 * 5) //5 minute intervals

        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute]
        formatter.unitsStyle = .full

        let title = formatter.string(from: time)!

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left

        return NSAttributedString(string: title,
                                  attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let selectedRow = scheduleTable.indexPathForSelectedRow {

            let weekDayIndex = (selectedRow.row + Calendar.current.firstWeekday - 1) % 7
            newSchedule.workTimeStartingOnSunday[weekDayIndex] = timeFromTimePicker()

            let detailText = composeCellDetailText(indexPath: scheduleTable.indexPathForSelectedRow!)

            scheduleTable.cellForRow(at: selectedRow)?.detailTextLabel?.text = detailText
        }
    }

    private func timeFromTimePicker() -> TimeInterval {
        let hours = timePicker.selectedRow(inComponent: 0)
        let minutes = timePicker.selectedRow(inComponent: 1) * 5
        let totalMinutes = hours * 60 + minutes
        let totalSeconds = totalMinutes * 60
        return TimeInterval(totalSeconds)
    }

    //Function to create detail text
}

extension ScheduleViewController: ScheduleViewControllerProtocol {
    func updateTable(with schedule: StoredSchedule) {
        self.newSchedule = ScheduleInput(from: schedule)
        scheduleTable.reloadData()
    }
}
