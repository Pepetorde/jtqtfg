//
//  SettingsPresenter.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import SafariServices

class SettingsPresenter {

    weak private var view: SettingsViewControllerProtocol?
    var scheduleView: ScheduleViewControllerProtocol?
    var dayChangeView: DayChangeViewControllerProtocol?
    private let router: SettingsRouterProtocol
    private let interactor: SettingsInteractorProtocol

    init(view: SettingsViewControllerProtocol, router: SettingsRouterProtocol, interactor: SettingsInteractorProtocol) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
}

extension SettingsPresenter: SettingsPresenterProtocol {

    func scheduleButtonSelected() {
        router.presentScheduleViewController(from: view, presenter: self)
    }

    func dayChangeButtonSelected() {
        router.presentDayChangeViewController(from: view, presenter: self)
    }

    func icons8LinkPressed() {
        if let url = URL(string: "https://icons8.com") {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:])
            }
        }
    }

    func retrieveSchedule() {
        interactor.retrieveSchedule()
    }

    func updateSchedule(newSchedule: ScheduleInput) {
        interactor.updateSchedule(newSchedule: newSchedule)
    }

    func retrieveDayChange() {
        interactor.retrieveDayChange()
    }

    func updateDayChange(newDayChange: DayChange) {
        interactor.updateDayChange(newDayChange: newDayChange)
    }
}

extension SettingsPresenter: SettingsInteractorOutputProtocol {

    func updateScheduleDidSuccess() {

    }

    func updateScheduleDidFail() {

    }

    func retrieveScheduleDidSuccess(retrievedSchedule: StoredSchedule) {
        scheduleView?.updateTable(with: retrievedSchedule)
    }

    func retrieveScheduleDidFail() {

    }

    func updateDayChangeDidSuccess() {

    }

    func updateDayChangeDidFail() {

    }

    func retrieveDayChangeDidSuccess(retrievedDayChange: StoredDayChange) {
        view?.updateDayChangeRow(with: retrievedDayChange)
        dayChangeView?.updateDayChangeField(with: retrievedDayChange)
    }

    func retrieveDayChangeDidFail() {

    }
}
