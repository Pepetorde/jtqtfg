//
//  SettingsProtocols.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit

protocol SettingsRouterProtocol: class {
    func presentScheduleViewController(from previousViewController: UIViewController?,
                                       presenter: SettingsPresenterProtocol)
    func presentDayChangeViewController(from previousViewController: UIViewController?,
                                        presenter: SettingsPresenterProtocol)
}

protocol SettingsPresenterProtocol: class {
    var scheduleView: ScheduleViewControllerProtocol? { get set }
    var dayChangeView: DayChangeViewControllerProtocol? { get set }

    func scheduleButtonSelected()
    func dayChangeButtonSelected()
    func icons8LinkPressed()

    func retrieveSchedule()
    func updateSchedule(newSchedule: ScheduleInput)

    func retrieveDayChange()
    func updateDayChange(newDayChange: DayChange)
}

protocol SettingsInteractorProtocol: class {
    func retrieveSchedule()
    func updateSchedule(newSchedule: ScheduleInput)

    func retrieveDayChange()
    func updateDayChange(newDayChange: DayChange)
}

protocol SettingsInteractorOutputProtocol: class {
    func updateScheduleDidSuccess()
    func updateScheduleDidFail()
    func retrieveScheduleDidSuccess(retrievedSchedule: StoredSchedule)
    func retrieveScheduleDidFail()

    func updateDayChangeDidSuccess()
    func updateDayChangeDidFail()
    func retrieveDayChangeDidSuccess(retrievedDayChange: StoredDayChange)
    func retrieveDayChangeDidFail()
}

protocol SettingsDataSourceProtocol: class {
    func retrieveSchedule()
    func updateSchedule(newSchedule: ScheduleInput)

    func retrieveDayChange()
    func updateDayChange(newDayChange: DayChange)
}

protocol SettingsDataSourceOutputProtocol: class {
    func updateScheduleDidSuccess()
    func updateScheduleDidFail()
    func retrieveScheduleDidSuccess(retrievedSchedule: StoredSchedule)
    func retrieveScheduleDidFail()

    func updateDayChangeDidSuccess()
    func updateDayChangeDidFail()
    func retrieveDayChangeDidSuccess(retrievedDayChange: StoredDayChange)
    func retrieveDayChangeDidFail()
}

protocol SettingsViewControllerProtocol: UIViewController {
    func updateDayChangeRow(with dayChange: StoredDayChange)
}

protocol ScheduleViewControllerProtocol: UIViewController {
    func updateTable(with schedule: StoredSchedule)
}

protocol DayChangeViewControllerProtocol: UIViewController {
    func updateDayChangeField(with dayChange: StoredDayChange)
}
