//
//  SettingsRouter.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import UIKit

class SettingsRouter: NestedTabBarViewRouterProtocol {

    var tabIcon: UIImage = UIImage(named: "settings")!
    var tabTitle: String = NSLocalizedString("settings.tabTitle", comment: "")

    let mainRouter: MainRouterInterface?

    init(mainRouter: MainRouterInterface? = nil) {
        self.mainRouter = mainRouter
    }

    static func createModule(mainRouter: MainRouter? = nil) -> UIViewController {
        let view = SettingsViewController()
        let router = SettingsRouter(mainRouter: mainRouter)
        let dataSource = SettingsDataSource()
        let interactor = SettingsInteractor(dataSource: dataSource)
        let presenter = SettingsPresenter(view: view, router: router, interactor: interactor)

        view.presenter = presenter
        interactor.presenter = presenter
        dataSource.interactor = interactor

        return view
    }

    func getAnidatedTabBarModule() -> UIViewController {
        let view = SettingsViewController()
        let router = SettingsRouter(mainRouter: mainRouter)
        let dataSource = SettingsDataSource()
        let interactor = SettingsInteractor(dataSource: dataSource)
        let presenter = SettingsPresenter(view: view, router: router, interactor: interactor)

        view.presenter = presenter
        interactor.presenter = presenter
        dataSource.interactor = interactor

        return view
    }
}

extension SettingsRouter: SettingsRouterProtocol {
    func presentScheduleViewController(from previousViewController: UIViewController?,
                                       presenter: SettingsPresenterProtocol) {
        let scheduleViewController = ScheduleViewController()
        scheduleViewController.presenter = presenter
        presenter.scheduleView = scheduleViewController

        previousViewController?.navigationController?.pushViewController(scheduleViewController, animated: true)
    }

    func presentDayChangeViewController(from previousViewController: UIViewController?,
                                        presenter: SettingsPresenterProtocol) {
        let dayChangeViewController = DayChangeViewController()
        dayChangeViewController.presenter = presenter
        presenter.dayChangeView = dayChangeViewController

        previousViewController?.navigationController?.pushViewController(dayChangeViewController, animated: true)
    }
}
