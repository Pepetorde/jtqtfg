//
//  WorkProtocols.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit

protocol WorkRouterProtocol: class {
    func presentWorkFragmentViewController(from previousViewController: UIViewController?,
                                           presenter: WorkPresenterProtocol,
                                           workFragment: WorkFragment)
    func popViewController(from navigationController: UINavigationController?)
}

protocol WorkPresenterProtocol: class {
    func viewDidAppear()
    func workFragmentSelected(_ workFragment: WorkFragment)
    func completeButtonPressed(workFragment: WorkFragment, newWorkTime: TimeInterval)
    func amountButtonPressed()
    func ratioButtonPressed()
    func realDay(ofDate date: Date) -> Date
}

protocol WorkInteractorOutputProtocol: class {

    func retrieveDayChangeDidSuccess(retrievedDayChange: StoredDayChange)
    func retrieveDayChangeDidFail()

    func retrieveCalendarDidSuccess(retrievedCalendar: WorkCalendar)
    func retrieveCalendarDidFail()

    func retrieveScenarioDidSuccess(scenario: Scenario)
    func retrieveScenarioDataDidFail()

    func updateTaskDidSuccess()
    func updateTaskDidFail()
}

protocol WorkInteractorProtocol: class {
    func retrieveDayChange()
    func retrieveCalendar()
    func retrieveScenario(after date: Date)
    func updateCalendar(calendar: WorkCalendar)
    func updateTask(task: Task)
}

protocol WorkDataSourceOutputProtocol: class {

    func retrieveDayChangeDidSuccess(retrievedDayChange: StoredDayChange)
    func retrieveDayChangeDidFail()

    func retrieveCalendarDidSuccess(calendarResults: [StoredWorkCalendar])
    func retrieveCalendarDidFail()

    func retrieveTasksDidSucceed(retrievedTasks: [StoredTask])
    func retrieveTasksDidFail()

    func retrieveScheduleDidSuccess(retrievedSchedule: StoredSchedule)
    func retrieveScheduleDidFail()
    func retrieveExceptionsDidSuccess(retrievedExceptions: [StoredException])
    func retrieveExceptionsDidFail()

    func updateTaskDidSuccess()
    func updateTaskDidFail()
}

protocol WorkDataSourceProtocol: class {

    func retrieveDayChange()
    func retrieveCalendar()
    func retrieveTasks(after date: Date)
    func retrieveSchedule()
    func retrieveExceptions(after date: Date)
    func deleteCalendar()
    func saveCalendar(calendar: WorkCalendar)
    func updateTask(task: Task)
}

protocol WorkViewControllerProtocol: UIViewController {
    func updateView()
    func updateView(with calendar: WorkCalendar)
    func hideLoadingIndicator()
}

protocol WorkFragmentViewControllerProtocol: class {

}
