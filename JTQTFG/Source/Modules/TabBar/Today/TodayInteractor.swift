//
//  WorkInteractor.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 08/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation

class WorkInteractor: WorkInteractorProtocol {

    var presenter: WorkInteractorOutputProtocol?
    private let dataSource: WorkDataSourceProtocol

    var date: Date?
    var tasks: [StoredTask]?
    var schedule: StoredSchedule?
    var exceptions: [StoredException]?

    init(dataSource: WorkDataSourceProtocol) {
        self.dataSource = dataSource
    }
}

extension WorkInteractor {

    func retrieveDayChange() {
        dataSource.retrieveDayChange()
    }

    func retrieveCalendar() {
        dataSource.retrieveCalendar()
    }

    func retrieveScenario(after date: Date) {
        self.date = date
        dataSource.retrieveTasks(after: date)
    }

    func updateCalendar(calendar: WorkCalendar) {
        dataSource.deleteCalendar()
        dataSource.saveCalendar(calendar: calendar)
    }

    func updateTask(task: Task) {
        dataSource.updateTask(task: task)
    }
}

extension WorkInteractor: WorkDataSourceOutputProtocol {

    func retrieveDayChangeDidSuccess(retrievedDayChange: StoredDayChange) {
        presenter?.retrieveDayChangeDidSuccess(retrievedDayChange: retrievedDayChange)
    }

    func retrieveCalendarDidSuccess(calendarResults: [StoredWorkCalendar]) {
        if calendarResults.isEmpty {
            presenter?.retrieveCalendarDidFail()
        } else {
            let calendar = WorkCalendar(from: calendarResults[0])
            presenter?.retrieveCalendarDidSuccess(retrievedCalendar: calendar)
        }
    }

    func retrieveTasksDidSucceed(retrievedTasks: [StoredTask]) {
        self.tasks = retrievedTasks
        dataSource.retrieveSchedule()
    }

    func retrieveScheduleDidSuccess(retrievedSchedule: StoredSchedule) {
        self.schedule = retrievedSchedule
        dataSource.retrieveExceptions(after: self.date!)
    }

    func retrieveExceptionsDidSuccess(retrievedExceptions: [StoredException]) {
        self.exceptions = retrievedExceptions
        generateScenario()
    }

    private func generateScenario() {
        let scenarioGenerator = ScenarioGenerator(date: self.date!,
                                                  tasks: self.tasks!,
                                                  schedule: self.schedule!,
                                                  exceptions: exceptions!)
        let scenario = scenarioGenerator.generateScenario()
        presenter?.retrieveScenarioDidSuccess(scenario: scenario)
    }

    func updateTaskDidSuccess() {
        presenter?.updateTaskDidSuccess()
    }

    func retrieveDayChangeDidFail() {
        presenter?.retrieveDayChangeDidFail()
    }

    func retrieveCalendarDidFail() {
        presenter?.retrieveCalendarDidFail()
    }

    func retrieveTasksDidFail() {
        presenter?.retrieveScenarioDataDidFail()
    }

    func retrieveScheduleDidFail() {
        presenter?.retrieveScenarioDataDidFail()
    }

    func retrieveExceptionsDidFail() {
        presenter?.retrieveScenarioDataDidFail()
    }

    func updateTaskDidFail() {
        presenter?.updateTaskDidFail()
    }
}
