//
//  WorkDataSource.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 08/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class WorkDataSource: WorkDataSourceProtocol {

    var interactor: WorkDataSourceOutputProtocol?

    weak var appDelegate: AppDelegate?
    let managedContext: NSManagedObjectContext?

    init() {
        self.appDelegate = UIApplication.shared.delegate as? AppDelegate
        self.managedContext = appDelegate?.persistentContainer.viewContext
    }
}

extension WorkDataSource {

    func retrieveDayChange() {
        let fetchRequest: NSFetchRequest<StoredDayChange> = StoredDayChange.fetchRequest()

        do {
            let results = try managedContext?.fetch(fetchRequest)

            UserDefaults.standard.set(false, forKey: "dayChangeHasChanged")
            interactor?.retrieveDayChangeDidSuccess(retrievedDayChange: results![0])
        } catch let error as NSError {
            print("No ha sido posible cargar \(error), \(error.userInfo)")
            interactor?.retrieveDayChangeDidFail()
        }
    }

    func retrieveCalendar() {
        let fetchRequest: NSFetchRequest<StoredWorkCalendar> = StoredWorkCalendar.fetchRequest()

        do {
            let results = try managedContext?.fetch(fetchRequest)

            interactor?.retrieveCalendarDidSuccess(calendarResults: results!)
        } catch let error as NSError {
            print("No ha sido posible cargar \(error), \(error.userInfo)")
            interactor?.retrieveCalendarDidFail()
        }
    }

    func retrieveTasks(after date: Date) {
        let fetchRequest: NSFetchRequest<StoredTask> = StoredTask.fetchRequest()
        let predicate = NSPredicate(format: "deadline >= %@", date as NSDate)
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "deadline", ascending: true)]

        do {
            let results = try managedContext?.fetch(fetchRequest)
            interactor?.retrieveTasksDidSucceed(retrievedTasks: results!)
        } catch let error as NSError {
            print("No ha sido posible cargar \(error), \(error.userInfo)")
            interactor?.retrieveTasksDidFail()
        }
    }

    func retrieveSchedule() {
        let fetchRequest: NSFetchRequest<StoredSchedule> = StoredSchedule.fetchRequest()

        do {
            let results = try managedContext?.fetch(fetchRequest)
            interactor?.retrieveScheduleDidSuccess(retrievedSchedule: results![0])
        } catch let error as NSError {
            print("No ha sido posible cargar \(error), \(error.userInfo)")
            interactor?.retrieveScheduleDidFail()
        }
    }

    func retrieveExceptions(after date: Date) {
        let fetchRequest: NSFetchRequest<StoredException> = StoredException.fetchRequest()
        let predicate = NSPredicate(format: "date >= %@", date as NSDate)
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]

        do {
            let results = try managedContext?.fetch(fetchRequest)
            interactor?.retrieveExceptionsDidSuccess(retrievedExceptions: results ?? [StoredException]())
        } catch let error as NSError {
            print("No ha sido posible cargar \(error), \(error.userInfo)")
            interactor?.retrieveExceptionsDidFail()
        }
    }

    func updateTask(task: Task) {
        let storedTask = retrieveTask(task: task)
        storedTask.workTime = task.workTime

        do {
            try managedContext?.save()
            interactor?.updateTaskDidSuccess()
        } catch let error as NSError {
            print("No ha sido posible actualizar \(error), \(error.userInfo)")
            interactor?.updateTaskDidFail()
        }
    }

    func deleteCalendar() {
        let fetchRquest: NSFetchRequest<StoredWorkCalendar> = StoredWorkCalendar.fetchRequest()

        do {
            let results = try managedContext?.fetch(fetchRquest)
            for result in results! {
                managedContext?.delete(result)
            }
            try managedContext?.save()
        } catch let error as NSError {
            print("No ha sido posible guardar \(error), \(error.userInfo)")
        }
    }

    func saveCalendar(calendar: WorkCalendar) {
        let storedCalendar = StoredWorkCalendar(entity: StoredWorkCalendar.entity(), insertInto: managedContext)
        let storedWorkDays = createWorkDays(for: calendar)
        let storedNotFittingTasks = createNotFittingTasks(for: calendar)

        storedCalendar.addToWorkDays(storedWorkDays)
        storedCalendar.addToDoNotFit(storedNotFittingTasks)

        do {
            try managedContext?.save()
            UserDefaults.standard.set(Date(), forKey: "lastSchedulingDate")
            UserDefaults.standard.set(false, forKey: "scenarioHasChanged")
            interactor?.retrieveCalendarDidSuccess(calendarResults: [storedCalendar])
        } catch let error as NSError {
            print("No ha sido posible guardar \(error), \(error.userInfo)")
            interactor?.retrieveCalendarDidFail()
        }
    }

    private func createWorkDays(for calendar: WorkCalendar) -> NSSet {
        var storedWorkDays = [StoredWorkDay]()

        for workDay in calendar.workDays {
            let storedWorkday = StoredWorkDay(entity: StoredWorkDay.entity(), insertInto: managedContext)
            let storedWorkFragments = createWorkFragments(for: workDay)

            storedWorkday.date = workDay.date
            storedWorkday.availableTime = workDay.availableTime
            storedWorkday.addToWork(storedWorkFragments)
            storedWorkday.optional = workDay.optional

            storedWorkDays.append(storedWorkday)
        }

        return NSSet(array: storedWorkDays)
    }

    private func createWorkFragments(for workDay: WorkDay) -> NSSet {
        var storedWorkFragments = [StoredWorkFragment]()

        for workFragment in workDay.work {
            let storedWorkFragment = StoredWorkFragment(entity: StoredWorkFragment.entity(),
                                                        insertInto: managedContext)
            let storedTask = retrieveTask(task: workFragment.task)

            storedWorkFragment.task = storedTask
            storedWorkFragment.workTime = workFragment.workTime
            storedWorkFragment.done = workFragment.done

            storedWorkFragments.append(storedWorkFragment)
        }

        return NSSet(array: storedWorkFragments)
    }

    private func createNotFittingTasks(for calendar: WorkCalendar) -> NSSet {
        var storedNotFittingTasks = [StoredNotFittingTask]()

        for notFittingTask in calendar.doNotFit {
            let storedNotFittingTask = StoredNotFittingTask(entity: StoredNotFittingTask.entity(),
                                                            insertInto: managedContext)
            let storedTask = retrieveTask(task: notFittingTask.task)
            storedNotFittingTask.task = storedTask
            storedNotFittingTask.howMuch = notFittingTask.howMuch

            storedNotFittingTasks.append(storedNotFittingTask)
        }

        return NSSet(array: storedNotFittingTasks)
    }

    private func retrieveTask(task: Task) -> StoredTask {
        let fetchRequest: NSFetchRequest<StoredTask> = StoredTask.fetchRequest()
        let namePredicate = NSPredicate(format: "name == %@", task.name)
        let startDatePredicate = NSPredicate(format: "startDate == %@", task.startDate as NSDate)
        let deadlinePredicate = NSPredicate(format: "deadline == %@", task.deadline as NSDate)

        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [namePredicate,
                                                                            startDatePredicate,
                                                                            deadlinePredicate])
        fetchRequest.predicate = predicate

        do {
            let results = try managedContext?.fetch(fetchRequest)
            return results![0]
        } catch let error as NSError {
            print("No ha sido posible cargar \(error), \(error.userInfo)")
            return StoredTask()
        }
    }
}
