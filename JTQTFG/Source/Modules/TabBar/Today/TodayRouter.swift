//
//  WorkRouter.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import UIKit

class WorkRouter: NestedTabBarViewRouterProtocol {

    var tabIcon: UIImage = UIImage(named: "muscles")!
    var tabTitle: String = NSLocalizedString("work.tabTitle", comment: "")

    let mainRouter: MainRouterInterface?

    init(mainRouter: MainRouterInterface? = nil) {
        self.mainRouter = mainRouter
    }

    static func createModule(mainRouter: MainRouter? = nil) -> UIViewController {
        let view = WorkViewController()
        let router = WorkRouter(mainRouter: mainRouter)
        let dataSource = WorkDataSource()
        let interactor = WorkInteractor(dataSource: dataSource)
        let presenter = WorkPresenter(view: view, router: router, interactor: interactor)

        view.presenter = presenter
        interactor.presenter = presenter
        dataSource.interactor = interactor

        return view
    }

    func getAnidatedTabBarModule() -> UIViewController {
        let view = WorkViewController()
        let router = WorkRouter(mainRouter: mainRouter)
        let dataSource = WorkDataSource()
        let interactor = WorkInteractor(dataSource: dataSource)
        let presenter = WorkPresenter(view: view, router: router, interactor: interactor)

        view.presenter = presenter
        interactor.presenter = presenter
        dataSource.interactor = interactor

        return view
    }
}

extension WorkRouter: WorkRouterProtocol {
    func presentWorkFragmentViewController(from previousViewController: UIViewController?,
                                           presenter: WorkPresenterProtocol,
                                           workFragment: WorkFragment) {
        let workFragmentViewController = WorkFragmentViewController(workFragment: workFragment)
        workFragmentViewController.presenter = presenter
        previousViewController?.navigationController?.pushViewController(workFragmentViewController, animated: true)
    }

    func popViewController(from navigationController: UINavigationController?) {
        navigationController?.popViewController(animated: true)
    }
}
