//
//  WorkViewControllerCalendar.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 27/05/2020.
//  Copyright © 2020 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import JTAppleCalendar

extension WorkViewController: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource {
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        let today = presenter!.realDay(ofDate: Date())
        let endDate = workCalendar?.lastWorkDay?.date ?? today

        return ConfigurationParameters(startDate: today, endDate: endDate, generateOutDates: .tillEndOfRow)
    }

    func calendar(_ calendar: JTAppleCalendarView,
                  cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(
            withReuseIdentifier: "dateCell", for: indexPath) as? DateCell ?? JTAppleCell()
        self.calendar(calendar, willDisplay: cell, forItemAt: date, cellState: cellState, indexPath: indexPath)
        return cell
    }

    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell,
                  forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        updateCell(cell, cellState: cellState)
    }

    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        updateCell(cell, cellState: cellState)
        selectedDayWorkTable.reloadData()
    }

    func calendar(_ calendar: JTAppleCalendarView,
                  didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        updateCell(cell, cellState: cellState)
    }

    func updateCell(_ cell: JTAppleCell?, cellState: CellState) {
        guard let customCell = cell as? DateCell  else { return }
        customCell.dateLabel.text = cellState.text
        updateCellHeat(cell: customCell, cellState: cellState)
        updateCellTextColor(cell: customCell, cellState: cellState)
        updateCellSelection(cell: customCell, cellState: cellState)
        updateCellToday(cell: customCell, cellState: cellState)
    }

    private func updateCellHeat(cell: DateCell, cellState: CellState) {
        cell.heatView.layer.cornerRadius = cell.bounds.width/2

        let cellDate = cellState.date.startOfDay
        let cellWorkDay = workCalendar?.workDays.first(where: {$0.date == cellDate})
        let cellWork: [WorkFragment]

        if cellWorkDay != nil {
            cellWork = cellWorkDay!.work
        } else {
            cellWork = []
        }

        if cellState.dateBelongsTo != .thisMonth || cellWork.isEmpty || cellWorkDay == nil {
            cell.heatView.isHidden = true
        } else {
            let heatModeIsAmount = UserDefaults.standard.bool(forKey: "popoverModeIsAmount")
            let busyRatio = getBusyRatio(for: cellWorkDay!, heatModeIsAmount: heatModeIsAmount)

            if busyRatio != 0 {
                cell.heatView.isHidden = false
                cell.heatView.backgroundColor = cell.heatColorForRatio(busyRatio)
            } else {
                cell.heatView.isHidden = true
            }
        }
    }

    private func getBusyRatio(for workDay: WorkDay, heatModeIsAmount: Bool) -> Double {
        let totalWork = workDay.totalWorkTime
        let availableTime = workDay.availableTime

        if heatModeIsAmount {
            return getBusyRatioForAmountMode(work: totalWork)
        } else {
            return getBusyRatioForOccupiedRatio(work: totalWork, availableTime: availableTime)
        }
    }

    private func getBusyRatioForAmountMode(work: TimeInterval) -> Double {
        if self.busiestDayWork != 0 {
            let ratio = work/self.busiestDayWork!
            if ratio > 1 {
                return 1
            } else {
                return ratio
            }
        } else {
            return 0
        }
    }

    private func getBusyRatioForOccupiedRatio(work: TimeInterval, availableTime: TimeInterval) -> Double {
        if availableTime != 0 {
            let ratio = work/availableTime
            if ratio > 1 {
                return 1
            } else {
                return ratio
            }
        } else {
            return 0
        }
    }

    private func updateCellTextColor(cell: DateCell, cellState: CellState) {
        if cellState.dateBelongsTo == .thisMonth {
            if cell.heatView.isHidden {
                cell.dateLabel.textColor = .black
                cell.dateLabel.font = .systemFont(ofSize: 17)
                cell.dateLabel.noSoftShadow()
            } else {
                cell.dateLabel.textColor = .white
                cell.dateLabel.font = .systemFont(ofSize: 17, weight: .heavy)
                cell.dateLabel.softShadow()
            }
        } else {
            cell.dateLabel.textColor = .gray
            cell.dateLabel.font = .systemFont(ofSize: 17)
            cell.dateLabel.noSoftShadow()
        }
    }

    private func updateCellSelection(cell: DateCell, cellState: CellState) {
        if cellState.isSelected && cellState.dateBelongsTo == .thisMonth {
            cell.selectedView.layer.borderWidth = 2
            cell.selectedView.layer.borderColor = UIColor.lightGray.cgColor
            cell.selectedView.layer.cornerRadius =  5
            cell.selectedView.isHidden = false
        } else {
            cell.selectedView.isHidden = true
        }
    }

    private func updateCellToday(cell: DateCell, cellState: CellState) {
        let realDate = presenter!.realDay(ofDate: Date())
        if realDate == cellState.date.startOfDay {
            cell.todayView.isHidden = false
            cell.todayView.layer.borderWidth = 2.5
            cell.todayView.layer.borderColor = UIColor.systemBlue.cgColor
            cell.todayView.layer.cornerRadius =  5
        } else {
            cell.todayView.isHidden = true
        }
    }

    func calendar(_ calendar: JTAppleCalendarView,
                  shouldSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) -> Bool {
        guard let customCell = cell as? DateCell  else { return true}
        if cellState.dateBelongsTo == .thisMonth && !customCell.heatView.isHidden {
            return true
        } else {
            return false
        }
    }

    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        updateMonthLabel()
        updateMonthButtons()
    }

    private func updateMonthLabel() {
        let currentMonthDate = calendarView.visibleDates().monthDates.first?.date
        let lowerCaseMonthName = currentMonthDate?.monthLocalizedString() ?? ""
        let firstUpperCaseMonthName = lowerCaseMonthName.prefix(1).uppercased() + lowerCaseMonthName.dropFirst()
        monthLabel.text = firstUpperCaseMonthName
    }

    private func updateMonthButtons() {
        if calendarView.currentSection() == 0 {
            previousMonthButton.isEnabled = false
        } else {
            previousMonthButton.isEnabled = true
        }
        if calendarView.currentSection() == calendarView.numberOfSections - 1 {
            nextMonthButton.isEnabled = false
        } else {
            nextMonthButton.isEnabled = true
        }
    }
}
