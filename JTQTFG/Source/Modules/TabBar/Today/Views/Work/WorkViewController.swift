//
//  WorkViewController.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import UIKit
import JTAppleCalendar

class WorkViewController: ParentViewController {

    @IBOutlet weak var notEnoughTimeView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var calendarPageView: UIView!

    @IBOutlet weak var previousMonthButton: UIButton!
    @IBOutlet weak var nextMonthButton: UIButton!
    @IBOutlet weak var heatmapModeButton: UIButton!
    @IBOutlet weak var calendarView: JTAppleCalendarView!

    @IBOutlet weak var selectedDayWorkTable: UITableView!
    @IBOutlet weak var todayWorkTable: UITableView!
    @IBOutlet weak var notEnoughTimeTable: UITableView!

    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var weekday1Label: UILabel!
    @IBOutlet weak var weekDay2Label: UILabel!
    @IBOutlet weak var weekDay3Label: UILabel!
    @IBOutlet weak var weekDay4Label: UILabel!
    @IBOutlet weak var weekDay5Label: UILabel!
    @IBOutlet weak var weekDay6Label: UILabel!
    @IBOutlet weak var weekDay7Label: UILabel!
    @IBOutlet weak var notEnoughTimeTitleLabel: UILabel!
    @IBOutlet weak var notEnoughTimeSubtitleLabel: UILabel!
    @IBOutlet weak var notEnoughTimeInfoLabel: UILabel!

    var presenter: WorkPresenterProtocol?

    var workCalendar: WorkCalendar?
    var busiestDayWork: TimeInterval?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = NSLocalizedString("work.navTitle", comment: "")
        notEnoughTimeView.isHidden = true
        showLoadingIndicator()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.viewDidAppear()
    }

    override func viewWillDisappear(_ animated: Bool) {
        hideLoadingIndicator()
    }
}

extension WorkViewController {
    @IBAction func segmentedControlIndexChanged(_ sender: Any) {
        scrollView.scrollTo(horizontalPage: segmentedControl.selectedSegmentIndex, animated: true)
    }
}

extension WorkViewController {
    @IBAction func previousMonthDidTouchUpInside(_ sender: Any) {
        calendarView.scrollToSegment(.previous)
    }

    @IBAction func nextMonthDidTouchUpInside(_ sender: Any) {
        calendarView.scrollToSegment(.next)
    }

    @IBAction func heatmapModeDidTouchUpInside(_ sender: UIButton) {
        showHeatmapModePopover(from: sender)
    }

    private func showHeatmapModePopover(from button: UIButton) {
        let heatmapModePopoverViewController = instantiateHeatmapModePopover()
        let configuredHeatmapModePopover = configurePopover(heatmapModePopoverViewController)
        present(configuredHeatmapModePopover, animated: true, completion: nil)
    }

    private func instantiateHeatmapModePopover() -> UIViewController {
        let heatmapModePopoverViewController = HeatmapModePopoverViewController()
        heatmapModePopoverViewController.presenter = self.presenter
        heatmapModePopoverViewController.preferredContentSize = CGSize(width: 262, height: 146)
        heatmapModePopoverViewController.modalPresentationStyle = .popover
        return heatmapModePopoverViewController
    }

    private func configurePopover(_ popoverViewController: UIViewController) -> UIViewController {
        let popoverPresentationController = popoverViewController.popoverPresentationController!
        popoverPresentationController.permittedArrowDirections = .up
        popoverPresentationController.sourceView = self.calendarPageView
        popoverPresentationController.sourceRect = self.heatmapModeButton.frame
        popoverPresentationController.delegate = self
        return popoverViewController
    }
}

extension WorkViewController {
    private func configureView() {
        configureScrollView()
        configureCalendarView()
        configureSelectedDayWorkTable()
        configureTodayWorkTable()
        configureNotEnoughTimeTable()
        setTextsInLanguage()
        notEnoughTimeView.isHidden = true
    }

    private func configureScrollView() {
        scrollView.delegate = self
    }

    private func configureCalendarView() {
        let calendarCellNib = UINib(nibName: "DateCell", bundle: nil)
        calendarView.register(calendarCellNib, forCellWithReuseIdentifier: "dateCell")
        calendarView.calendarDelegate = self
        calendarView.calendarDataSource = self
        calendarView.scrollToDate(presenter!.realDay(ofDate: Date()))
    }

    private func configureSelectedDayWorkTable() {
        selectedDayWorkTable.dataSource = self
        selectedDayWorkTable.delegate = self
        selectedDayWorkTable.register(UITableViewCell.self, forCellReuseIdentifier: "workCell")
    }

    private func configureTodayWorkTable() {
        todayWorkTable.dataSource = self
        todayWorkTable.delegate = self
        todayWorkTable.register(UITableViewCell.self, forCellReuseIdentifier: "workCell")
    }

    private func configureNotEnoughTimeTable() {
        notEnoughTimeTable.dataSource = self
        notEnoughTimeTable.delegate = self
        notEnoughTimeTable.register(UITableViewCell.self, forCellReuseIdentifier: "notEnoughTimeCell")
    }

    private func setTextsInLanguage() {
        setWeekDayNames()
        segmentedControl.setTitle(NSLocalizedString("work.calendarSegmentedTitle", comment: ""), forSegmentAt: 0)
        segmentedControl.setTitle(NSLocalizedString("work.todaySegmentedTitle", comment: ""), forSegmentAt: 1)
        notEnoughTimeTitleLabel.text = NSLocalizedString("work.notEnoughTitle", comment: "")
        notEnoughTimeSubtitleLabel.text = NSLocalizedString("work.notEnoughSubtitle", comment: "")
        notEnoughTimeInfoLabel.text = NSLocalizedString("work.notEnoughInfo", comment: "")
    }

    private func setWeekDayNames() {
        let weekDayLabels = [weekday1Label, weekDay2Label, weekDay3Label, weekDay4Label,
                             weekDay5Label, weekDay6Label, weekDay7Label]
        let firstDayOfWeek = Calendar.current.firstWeekday
        let weekDaySimbols = Calendar.current.veryShortWeekdaySymbols
        for weekDayLabelIndex in 0..<weekDayLabels.count {
            let weekDaySymbolIndex = (weekDayLabelIndex + firstDayOfWeek - 1) % 7
            weekDayLabels[weekDayLabelIndex]?.text = weekDaySimbols[weekDaySymbolIndex]
        }
    }
}

extension WorkViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension WorkViewController: UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView == self.scrollView && decelerate == false {
            segmentedControl.selectedSegmentIndex = scrollView.currentPageIndex
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView {
            segmentedControl.selectedSegmentIndex = scrollView.currentPageIndex
        }
    }
}

extension WorkViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == selectedDayWorkTable {
            return selectedDayWorkTable(numberOfRowsInSection: section)
        } else if tableView == todayWorkTable {
            return todayWorkTable(numberOfRowsInSection: section)
        } else {
            return notEnoughTimeTable(numberOfRowsInSection: section)
        }
    }

    private func selectedDayWorkTable(numberOfRowsInSection section: Int) -> Int {
        if calendarView.selectedDates.count != 0 {
            let selectedDate = calendarView.selectedDates[0].startOfDay
            let workDay = workCalendar?.workDays.first {workday in workday.date.startOfDay == selectedDate}
            return workDay?.work.count ?? 0
        }
        return 0
    }

    private func todayWorkTable(numberOfRowsInSection section: Int) -> Int {
        let today = presenter?.realDay(ofDate: Date())
        let workDay = workCalendar?.workDays.first {workday in workday.date.startOfDay == today}
        return workDay?.work.count ?? 0
    }

    private func notEnoughTimeTable(numberOfRowsInSection section: Int) -> Int {
        return workCalendar?.doNotFit.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == selectedDayWorkTable {
            return selectedDayWorkTable(cellForRowAt: indexPath)
        } else if tableView == todayWorkTable {
            return todayWorkTable(cellForRowAt: indexPath)
        } else {
            return notEnoughTimeTable(cellForRowAt: indexPath)
        }
    }

    private func selectedDayWorkTable(cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "workCell")

        var work: [WorkFragment] = []

        if calendarView.selectedDates.count != 0 {
            let selectedDate = calendarView.selectedDates[0].startOfDay
            let workDay = workCalendar?.workDays.first {workday in workday.date.startOfDay == selectedDate}
            work = workDay?.work ?? []
        }

        if indexPath.row < work.count {
            if work[indexPath.row].done == false {
                cell.textLabel?.text = work[indexPath.row].task.name
            } else {
                let attributedString = NSMutableAttributedString(string: work[indexPath.row].task.name)
                let range = NSRange(location: 0, length: attributedString.length)
                attributedString.addAttribute(.strikethroughStyle, value: 2, range: range)
                cell.textLabel?.attributedText = attributedString
                cell.textLabel?.textColor = .lightGray
            }
            cell.detailTextLabel?.text = work[indexPath.row].workTime.hourMinuteLocalizedString(style: .full)
        }

        return cell
    }

    private func todayWorkTable(cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "workCell")

        let today = presenter?.realDay(ofDate: Date())
        let workDay = workCalendar?.workDays.first {workday in workday.date.startOfDay == today}
        let work = workDay?.work ?? []

        if indexPath.row < work.count {
            if work[indexPath.row].done == false {
                cell.textLabel?.text = work[indexPath.row].task.name
                cell.accessoryType = .disclosureIndicator
            } else {
                let attributedString = NSMutableAttributedString(string: work[indexPath.row].task.name)
                let range = NSRange(location: 0, length: attributedString.length)
                attributedString.addAttribute(.strikethroughStyle, value: 2, range: range)
                cell.textLabel?.attributedText = attributedString
                cell.textLabel?.textColor = .lightGray
            }
            cell.detailTextLabel?.text = work[indexPath.row].workTime.hourMinuteLocalizedString(style: .full)
        }

        return cell
    }

    private func notEnoughTimeTable(cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "notEnoughTimeCell")

        let notFittingTask = workCalendar!.doNotFit[indexPath.row]
        cell.textLabel?.text = notFittingTask.task.name
        cell.detailTextLabel?.text = notFittingTask.howMuch.hourMinuteLocalizedString(style: .full)

        return cell
    }

    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if tableView == todayWorkTable {
            let today = presenter?.realDay(ofDate: Date())
            let workDay = workCalendar?.workDays.first {workday in workday.date.startOfDay == today}
            let work = workDay?.work ?? []

            if indexPath.row < work.count && work[indexPath.row].done == true {
                return nil
            }
        }
        return indexPath
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == todayWorkTable {
            let today = presenter?.realDay(ofDate: Date())
            if let workDay = workCalendar?.workDays.first(where: {workDay in workDay.date == today}) {
                presenter?.workFragmentSelected(workDay.work[indexPath.row])
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension WorkViewController: WorkViewControllerProtocol {
    func updateView(with calendar: WorkCalendar) {
        self.workCalendar = calendar
        if !calendar.doNotFit.isEmpty {
            notEnoughTimeView.isHidden = false
            view.bringSubviewToFront(notEnoughTimeView)
            notEnoughTimeTable.reloadData()
        } else {
            view.sendSubviewToBack(notEnoughTimeView)
            notEnoughTimeView.isHidden = true
            self.busiestDayWork = calendar.busiestWorkDay?.totalWorkTime
            calendarView.reloadData()
            selectedDayWorkTable.reloadData()
            todayWorkTable.reloadData()
        }
    }

    func updateView() {
        calendarView.reloadData()
        selectedDayWorkTable.reloadData()
        todayWorkTable.reloadData()
        notEnoughTimeTable.reloadData()
    }
}

class DateCell: JTAppleCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var heatView: UIView!
    @IBOutlet weak var todayView: UIView!

    func heatColorForRatio(_ ratio: Double) -> UIColor {
        if ratio > 0 && ratio <= 0.5 {
            let green = CGFloat(1)
            let red = CGFloat(ratio * 0.6 / 0.5 + 0.4) // between 0 and 1
            return UIColor(red: red, green: green, blue: 0, alpha: 1)
        } else if ratio > 0.5 && ratio <= 1 {
            let green = CGFloat((1-ratio) * 0.8 / 0.5 + 0.2) //between 1 and 0
            let red = CGFloat(1)
            return UIColor(red: CGFloat(red), green: CGFloat(green), blue: 0, alpha: 1)
        } else {
            return .clear
        }
    }
}
