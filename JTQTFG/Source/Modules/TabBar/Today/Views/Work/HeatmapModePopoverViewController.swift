//
//  HeatmapModePopoverViewController.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 13/05/2020.
//  Copyright © 2020 José Tordesillas Quintana. All rights reserved.
//

import UIKit

class HeatmapModePopoverViewController: UIViewController {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var amountButton: UIButton!
    @IBOutlet weak var ratioButton: UIButton!

    var presenter: WorkPresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    @IBAction func amountButtonDidTouchUpInside(_ sender: Any) {
        presenter?.amountButtonPressed()
        dismiss(animated: true, completion: nil)
    }

    @IBAction func ratioButtonDidTouchUpInside(_ sender: Any) {
        presenter?.ratioButtonPressed()
        dismiss(animated: true, completion: nil)
    }

    private func configureView() {
        setTextsInLangage()
    }

    private func setTextsInLangage() {
        descriptionLabel.text = NSLocalizedString("heatmap.description", comment: "")
        amountButton.setTitle(NSLocalizedString("heatmap.amountButton", comment: ""), for: .normal)
        ratioButton.setTitle(NSLocalizedString("heatmap.ratioButton", comment: ""), for: .normal)
    }
}
