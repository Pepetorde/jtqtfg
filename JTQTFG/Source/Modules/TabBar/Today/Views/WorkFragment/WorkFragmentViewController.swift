//
//  WorkFragmentViewController.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 11/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit

class WorkFragmentViewController: ParentViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var workTimeHeader: UILabel!
    @IBOutlet weak var workTimeLabel: UILabel!
    @IBOutlet weak var timePicker: UIPickerView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var completeButton: UIButton!

    var workFragment: WorkFragment
    var presenter: WorkPresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        timePicker.delegate = self
        timePicker.dataSource = self
        configureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = workFragment.task.name
    }

    init(workFragment: WorkFragment) {
        self.workFragment = workFragment
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        self.workFragment = WorkFragment()
        super.init(coder: aDecoder)
    }
}

extension WorkFragmentViewController {
    private func configureView() {
        setContent()
        setTextsInLanguage()
    }

    private func setContent() {
        titleLabel.text = workFragment.task.name
        workTimeLabel.text = workFragment.workTime.hourMinuteLocalizedString(style: .full)
        setRemainingTimePicker()
    }

    private func setRemainingTimePicker() {
        let remainingTime: TimeInterval
        let taskWorkTime = workFragment.task.workTime
        remainingTime = taskWorkTime - workFragment.workTime

        setTimePickerFrom(timeInterval: remainingTime)
    }

    private func setTextsInLanguage() {
        workTimeHeader.text = NSLocalizedString("workFragment.workTime", comment: "")
        infoLabel.text = NSLocalizedString("workFragment.infoLabel", comment: "")
        completeButton.setTitle(NSLocalizedString("workFragment.complete", comment: ""), for: .normal)
    }

    private func setTimePickerFrom(timeInterval: TimeInterval) {
        let totalMinutes = Int(timeInterval / 60)
        let minutes = totalMinutes % 60 / 5
        let hours = (totalMinutes - minutes) / 60
        timePicker.selectRow(hours, inComponent: 0, animated: true)
        timePicker.selectRow(minutes, inComponent: 1, animated: true)
    }
}

extension WorkFragmentViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 { //hours
            return 200
        } else { //minutes
            return 60 / 5
        }
    }

    func pickerView(_ pickerView: UIPickerView,
                    attributedTitleForRow row: Int,
                    forComponent component: Int) -> NSAttributedString? {

        if component == 0 { //hours
            return attributedHoursTitleStringFor(row: row)
        } else { // minutes
            return attributedMinutesTitleStringFor(row: row)
        }
    }

    func attributedHoursTitleStringFor(row: Int) -> NSAttributedString {
        let time = TimeInterval(row * 60 * 60)

        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour]
        formatter.unitsStyle = .full

        let title = formatter.string(from: time)!

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .right
        paragraphStyle.tailIndent = -20

        return NSAttributedString(string: title,
                                  attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
    }

    func attributedMinutesTitleStringFor(row: Int) -> NSAttributedString {
        let time = TimeInterval(row * 60 * 5) //5 minute intervals

        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute]
        formatter.unitsStyle = .full

        let title = formatter.string(from: time)!

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left

        return NSAttributedString(string: title,
                                  attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
    }

    private func timeFromTimePicker() -> TimeInterval {
        let hours = timePicker.selectedRow(inComponent: 0)
        let minutes = timePicker.selectedRow(inComponent: 1) * 5
        let totalMinutes = hours * 60 + minutes
        let totalSeconds = totalMinutes * 60
        return TimeInterval(totalSeconds)
    }
}

extension WorkFragmentViewController {
    @IBAction func completeButtonDidTouchUpInside(_ sender: Any) {
        presenter?.completeButtonPressed(workFragment: workFragment, newWorkTime: timeFromTimePicker())
    }
}

extension WorkFragmentViewController: WorkFragmentViewControllerProtocol {

}
