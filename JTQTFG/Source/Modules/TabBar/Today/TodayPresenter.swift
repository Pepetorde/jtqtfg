//
//  WorkPresenter.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit

class WorkPresenter: WorkPresenterProtocol {

    weak private var view: WorkViewControllerProtocol?
    private let router: WorkRouterProtocol
    private let interactor: WorkInteractorProtocol

    var dayChange: StoredDayChange?
    var calendar: WorkCalendar?

    init(view: WorkViewControllerProtocol, router: WorkRouterProtocol, interactor: WorkInteractorProtocol) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
}

extension WorkPresenter {
    func viewDidAppear() {
        checkDayChangeIsUpToDate()
    }

    private func checkDayChangeIsUpToDate() {
        let hourHasChanged = UserDefaults.standard.bool(forKey: "dayChangeHasChanged")
        if hourHasChanged || self.dayChange == nil {
            interactor.retrieveDayChange()
        } else {
            checkPresenterHasCalendar()
        }
    }

    private func checkPresenterHasCalendar() {
        if calendar == nil {
            interactor.retrieveCalendar()
        } else {
            checkIfRescheduleIsNeeded()
        }
    }

    private func checkIfRescheduleIsNeeded() {
        let scenarioHasChanged = UserDefaults.standard.bool(forKey: "scenarioHasChanged")
        let dayHasChanged = hasDayChanged()
        let calendarIsMissing = self.calendar == nil

        if calendarIsMissing {
            rescheduleCalendar(after: realDay(ofDate: Date()))
        } else if dayHasChanged {
            askForReestimation()
        } else if scenarioHasChanged {
            rescheduleCalendar(after: realDay(ofDate: Date()))
        } else {
            view?.updateView(with: self.calendar!)
            view?.hideLoadingIndicator()
        }
    }

    private func hasDayChanged() -> Bool {
        let lastSchedulingDate = UserDefaults.standard.object(forKey: "lastSchedulingDate") as? Date ?? Date()
        let currentDate = Date()

        let lastSchedulingDay = realDay(ofDate: lastSchedulingDate)
        let currentDay = realDay(ofDate: currentDate)

        if currentDay == lastSchedulingDay {
            return false
        } else {
            return true
        }
    }

    func realDay(ofDate date: Date) -> Date {
        let dateDay: Date
        if dateBelongsToPreviousDay(date: date) {
            dateDay = date.previousDay.startOfDay
        } else {
            dateDay = date.startOfDay
        }
        return dateDay
    }

    private func dateBelongsToPreviousDay(date: Date) -> Bool {
        let dateHour = Calendar.current.component(.hour, from: date)
        let dateMinute = Calendar.current.component(.minute, from: date)

        if self.dayChange?.hour ?? 0 > dateHour {
            return true
        } else if self.dayChange?.hour ?? 0 == dateHour && self.dayChange?.minute ?? 0 > dateMinute {
            return true
        } else {
            return false
        }
    }

    private func rescheduleCalendar(after date: Date) {
        interactor.retrieveScenario(after: date)
    }

    private func askForReestimation() {
        //Futuro desarrollo
        rescheduleCalendar(after: realDay(ofDate: Date()))
    }

    func workFragmentSelected(_ workFragment: WorkFragment) {
        router.presentWorkFragmentViewController(from: view, presenter: self, workFragment: workFragment)
    }

    func completeButtonPressed(workFragment: WorkFragment, newWorkTime: TimeInterval) {
        router.popViewController(from: view?.navigationController)

        var task = workFragment.task
        task.workTime = newWorkTime

        var today = calendar?.workDays.first(where: {$0.date == realDay(ofDate: Date())})
        let todayIndex = calendar?.workDays.firstIndex(where: {$0.date == realDay(ofDate: Date())})

        var calendarWorkFragment = today?.work.first(where: {$0 == workFragment})
        let calendarWorkFragmentIndex = today?.work.firstIndex(where: {$0 == workFragment})

        calendarWorkFragment?.done = true
        today?.work[calendarWorkFragmentIndex!] = calendarWorkFragment!
        calendar?.workDays[todayIndex!] = today!

        interactor.updateTask(task: task)
    }

    func amountButtonPressed() {
        UserDefaults.standard.set(true, forKey: "popoverModeIsAmount")
        view?.updateView()
    }

    func ratioButtonPressed() {
        UserDefaults.standard.set(false, forKey: "popoverModeIsAmount")
        view?.updateView()
    }
}

extension WorkPresenter: WorkInteractorOutputProtocol {
    func retrieveDayChangeDidSuccess(retrievedDayChange: StoredDayChange) {
        self.dayChange = retrievedDayChange
        checkPresenterHasCalendar()
    }

    func retrieveCalendarDidSuccess(retrievedCalendar: WorkCalendar) {
        self.calendar = retrievedCalendar
        checkIfRescheduleIsNeeded()
    }

    func retrieveCalendarDidFail() {
        self.calendar = nil
        checkIfRescheduleIsNeeded()
    }

    func retrieveScenarioDidSuccess(scenario: Scenario) {
        let scheduler = Scheduler(scenario: scenario)
        let calendar = scheduler.generateCalendar()
        interactor.updateCalendar(calendar: calendar)
    }

    func retrieveDayChangeDidFail() {

    }

    func retrieveScenarioDataDidFail() {

    }

    func updateTaskDidSuccess() {
        interactor.updateCalendar(calendar: calendar!)
    }

    func updateTaskDidFail() {
    }
}
