//
//  ExceptionsRouter.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import UIKit

class ExceptionsRouter: NestedTabBarViewRouterProtocol {

    var tabIcon: UIImage = UIImage(named: "cocktail")!
    var tabTitle: String = NSLocalizedString("exceptions.tabTitle", comment: "")

    let mainRouter: MainRouterInterface?

    init(mainRouter: MainRouterInterface? = nil) {
        self.mainRouter = mainRouter
    }

    static func createModule(mainRouter: MainRouter? = nil) -> UIViewController {
        let view = ExceptionsViewController()
        let router = ExceptionsRouter(mainRouter: mainRouter)
        let dataSource = ExceptionsDataSource()
        let interactor = ExceptionsInteractor(dataSource: dataSource)
        let presenter = ExceptionsPresenter(view: view, router: router, interactor: interactor)

        view.presenter = presenter
        interactor.presenter = presenter
        dataSource.interactor = interactor

        return view
    }

    func getAnidatedTabBarModule() -> UIViewController {
        let view = ExceptionsViewController()
        let router = ExceptionsRouter(mainRouter: mainRouter)
        let dataSource = ExceptionsDataSource()
        let interactor = ExceptionsInteractor(dataSource: dataSource)
        let presenter = ExceptionsPresenter(view: view, router: router, interactor: interactor)

        view.presenter = presenter
        interactor.presenter = presenter
        dataSource.interactor = interactor

        return view
    }
}

extension ExceptionsRouter: ExceptionsRouterProtocol {
    func presentAddExceptionViewController(from previousViewController: UIViewController?,
                                           presenter: ExceptionsPresenterProtocol) {
        let addExceptionViewController = AddExceptionViewController()
        addExceptionViewController.presenter = presenter
        previousViewController?.navigationController?.pushViewController(addExceptionViewController, animated: true)
    }

    func presentEditExceptionViewController(from previousViewController: UIViewController?,
                                            presenter: ExceptionsPresenterProtocol,
                                            exceptionToBeEdited: StoredException) {
        let editExceptionViewController = EditExceptionViewController(exceptionToBeEdited: exceptionToBeEdited)
        editExceptionViewController.presenter = presenter
        previousViewController?.navigationController?.pushViewController(editExceptionViewController, animated: true)
    }

    func popViewController(from navigationController: UINavigationController?) {
        navigationController?.popViewController(animated: true)
    }
}
