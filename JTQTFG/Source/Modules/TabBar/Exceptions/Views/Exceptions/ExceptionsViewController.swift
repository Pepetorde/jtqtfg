//
//  ExceptionsViewController.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import UIKit

class ExceptionsViewController: ParentViewController {

    @IBOutlet weak var exceptionsTable: UITableView!

    var presenter: ExceptionsPresenterProtocol?

    var exceptions = [StoredException]()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.title = NSLocalizedString("exceptions.navTitle", comment: "")
        presenter?.retrieveExceptions()
    }

}

extension ExceptionsViewController {
    private func configureView() {
        setNavigationBar()
        configureTableView()
    }

    private func setNavigationBar() {
        let addExceptionButton = UIBarButtonItem(
            barButtonSystemItem: .add,
            target: self,
            action: #selector(addExceptionButtonDidTouchUpInside))
        navigationItem.rightBarButtonItem = addExceptionButton
    }

    private func configureTableView() {
        exceptionsTable.dataSource = self
        exceptionsTable.delegate = self
        let exceptionCellNib = UINib(nibName: "ExceptionsTableCell", bundle: nil)
        exceptionsTable.register(exceptionCellNib, forCellReuseIdentifier: "exceptionTableCell")
    }
}

extension ExceptionsViewController {
    @objc private func addExceptionButtonDidTouchUpInside() {
        presenter?.addExceptionButtonPressed()
    }
}

extension ExceptionsViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exceptions.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = exceptionsTable.dequeueReusableCell(withIdentifier: "exceptionTableCell") as? ExceptionTableCell {

            let exception = exceptions[indexPath.row]

            let localizedExceptionDate = DateFormatter.localizedString(
                from: exception.date ?? Date(),
                dateStyle: .long,
                timeStyle: .none)

            let timeFormatter = DateComponentsFormatter()
            timeFormatter.allowedUnits = [.hour, .minute]
            timeFormatter.unitsStyle = .short

            let localizedExceptionWorkTime = timeFormatter.string(from: TimeInterval(exception.workTime))

            let exceptionIsPast = exception.date! < Date().startOfDay

            if exceptionIsPast {
                let attributedString = NSMutableAttributedString(string: exception.name ?? "")
                let range = NSRange(location: 0, length: attributedString.length)
                attributedString.addAttribute(.strikethroughStyle, value: 2, range: range)
                cell.nameLabel.attributedText = attributedString
                cell.nameLabel.textColor = .lightGray
            } else {
                cell.nameLabel.attributedText = nil
                cell.nameLabel.textColor = .darkText
                cell.nameLabel.text = exception.name
            }

            cell.dateLabel.text = localizedExceptionDate
            cell.workTimeLabel.text = localizedExceptionWorkTime

            return cell
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedException = exceptions[indexPath.row]
        presenter?.didSelectException(selectedException)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView,
                   commit editingStyle: UITableViewCell.EditingStyle,
                   forRowAt indexPath: IndexPath) {
        presenter?.deleteException(exceptions[indexPath.row])
    }
}

extension ExceptionsViewController: ExceptionsViewControllerProtocol {
    func reloadTable(exceptions: [StoredException]) {
        self.exceptions = exceptions
        exceptionsTable.reloadData()
    }

    func removeException(exception: StoredException) {
        if let index = exceptions.firstIndex(of: exception) {
            exceptions.remove(at: index)
            exceptionsTable.beginUpdates()
            exceptionsTable.deleteRows(at: [IndexPath(row: index, section: 0)], with: .left)
            exceptionsTable.endUpdates()
        }
    }
}

class ExceptionTableCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var workTimeLabel: UILabel!
}
