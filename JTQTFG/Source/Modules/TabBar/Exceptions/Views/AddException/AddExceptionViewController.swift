//
//  AddExceptionViewController.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 07/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit

class AddExceptionViewController: ParentViewController {

    @IBOutlet weak var titleLabel: UITextField!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var workTimeLabel: UILabel!
    @IBOutlet weak var workTimeButton: UIButton!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var optionalLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var timePicker: UIPickerView!

    var presenter: ExceptionsPresenterProtocol?

    var newException = Exception()

    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.delegate = self
        datePicker.datePickerMode = .date
        timePicker.delegate = self
        timePicker.dataSource = self
        configureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = NSLocalizedString("addException.navTitle", comment: "")
        titleLabel.becomeFirstResponder()
        updateDateButton()
        updateWorkTimeButton()
    }
}

extension AddExceptionViewController {

    private func configureView() {
        setNavigationBar()
        setTextsInLanguage()
        setButtonsText()
    }

    private func setNavigationBar() {
        let backButton = UIBarButtonItem(barButtonSystemItem: .cancel,
                                         target: self,
                                         action: #selector(cancelButtonDidTouchUpInside))
        let addExceptionButton = UIBarButtonItem(barButtonSystemItem: .done,
                                            target: self,
                                            action: #selector(doneButtonDidTouchUpInside))

        navigationItem.rightBarButtonItem = addExceptionButton
        navigationItem.leftBarButtonItem = backButton
    }

    private func setTextsInLanguage() {
        titleLabel.placeholder = NSLocalizedString("addException.placeholder", comment: "")
        dateLabel.text = NSLocalizedString("addException.date", comment: "")
        dateButton.setTitle(NSLocalizedString("addException.dateButton", comment: ""), for: .normal)
        workTimeButton.setTitle(NSLocalizedString("addException.workTimeButton", comment: ""), for: .normal)
        workTimeLabel.text = NSLocalizedString("addException.workTime", comment: "")
        infoLabel.text = NSLocalizedString("addException.infoLabel", comment: "")
        optionalLabel.text = NSLocalizedString("addException.optionalLabel", comment: "")
    }

    private func setButtonsText() {
        updateDateButton()
        updateWorkTimeButton()
    }
}

extension AddExceptionViewController {

    @objc private func cancelButtonDidTouchUpInside() {
        tabBarController?.tabBar.isHidden = false
        presenter?.cancelButtonPressed()
    }

    @objc private func doneButtonDidTouchUpInside() {
        var fieldsAreCorrectlyFilled = true

        if newException.name == "" {
            titleLabel.errorWiggle()
            fieldsAreCorrectlyFilled = false
        }

        if fieldsAreCorrectlyFilled {
            tabBarController?.tabBar.isHidden = false
            presenter?.saveNewException(newException: newException)
        }
    }

    @IBAction func dateButtonDidTouchUpInside(_ sender: Any) {
        tabBarController?.tabBar.isHidden = true
        titleLabel.resignFirstResponder()

        datePicker.setDate(newException.date, animated: true)

        datePicker.minimumDate = Date()
        datePicker.maximumDate = nil
        datePicker.isHidden = false
        timePicker.isHidden = true
    }

    @IBAction func workTimeButtonDidTouchUpInside(_ sender: Any) {
        tabBarController?.tabBar.isHidden = true
        titleLabel.resignFirstResponder()

        setTimePickerFrom(timeInterval: newException.workTime)

        datePicker.isHidden = true
        timePicker.isHidden = false
    }

    @IBAction func optionalSwitchValueDidChange(_ sender: UISwitch) {
        tabBarController?.tabBar.isHidden = false
        titleLabel.resignFirstResponder()
        datePicker.isHidden = true
        timePicker.isHidden = true

        newException.optional = sender.isOn
    }

    @IBAction func datePickerDidChangeValue(_ sender: UIDatePicker) {
        newException.date = sender.date.startOfDay
        updateDateButton()
    }

    private func updateDateButton() {
        let localizedDate = DateFormatter.localizedString(
            from: newException.date,
            dateStyle: .full,
            timeStyle: .none)
        dateButton.setTitle(localizedDate, for: .normal)
    }

    private func updateWorkTimeButton() {
        let timeInterval = timeFromTimePicker()

        let localizedTimeInterval = timeInterval.hourMinuteLocalizedString(style: .full)
        workTimeButton.setTitle(localizedTimeInterval, for: .normal)
    }

    @IBAction func backgroundDidTouchUpInside(_ sender: Any) {
        tabBarController?.tabBar.isHidden = false
        datePicker.isHidden = true
        timePicker.isHidden = true
        titleLabel.resignFirstResponder()
    }
}

extension AddExceptionViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 { //hours
            return 200
        } else { //minutes
            return 60 / 5
        }
    }

    func pickerView(_ pickerView: UIPickerView,
                    attributedTitleForRow row: Int,
                    forComponent component: Int) -> NSAttributedString? {
        if component == 0 { //hours
            return attributedHoursTitleStringFor(row: row)
        } else { // minutes
            return attributedMinutesTitleStringFor(row: row)
        }
    }

    func attributedHoursTitleStringFor(row: Int) -> NSAttributedString {
        let time = TimeInterval(row * 60 * 60)

        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour]
        formatter.unitsStyle = .full

        let title = formatter.string(from: time)!

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .right
        paragraphStyle.tailIndent = -20

        return NSAttributedString(string: title,
                                  attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
    }

    func attributedMinutesTitleStringFor(row: Int) -> NSAttributedString {
        let time = TimeInterval(row * 60 * 5) //5 minute intervals

        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute]
        formatter.unitsStyle = .full

        let title = formatter.string(from: time)!

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left

        return NSAttributedString(string: title,
                                  attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        newException.workTime = timeFromTimePicker()
        updateWorkTimeButton()
    }

    private func setTimePickerFrom(timeInterval: TimeInterval) {
        let totalMinutes = Int(timeInterval / 60)
        let minutes = totalMinutes % 60 / 5
        let hours = (totalMinutes - minutes) / 60
        timePicker.selectRow(hours, inComponent: 0, animated: true)
        timePicker.selectRow(minutes, inComponent: 1, animated: true)
    }

    private func timeFromTimePicker() -> TimeInterval {
        let hours = timePicker.selectedRow(inComponent: 0)
        let minutes = timePicker.selectedRow(inComponent: 1) * 5
        let totalMinutes = hours * 60 + minutes
        let totalSeconds = totalMinutes * 60
        return TimeInterval(totalSeconds)
    }
}

extension AddExceptionViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        datePicker.isHidden = true
        timePicker.isHidden = true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        newException.name = textField.text ?? ""
        return true
    }

    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let textBeforeChange = textField.text as NSString?
        let textAfterChange = textBeforeChange?.replacingCharacters(in: range, with: string)
        newException.name = textAfterChange ?? ""

        return true
    }
}

extension AddExceptionViewController: AddExceptionViewControllerProtocol {

}
