//
//  EditExceptionViewController.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 07/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit

class EditExceptionViewController: ParentViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var workTimeLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var workTimeButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var titleLabel: UITextField!
    @IBOutlet weak var optionalSwitch: UISwitch!
    @IBOutlet weak var optionalLabel: UILabel!
    @IBOutlet weak var timePicker: UIPickerView!

    var presenter: ExceptionsPresenterProtocol?

    var originalException: StoredException
    var newException: Exception

    init(exceptionToBeEdited: StoredException) {
        self.originalException = exceptionToBeEdited
        self.newException = Exception(from: exceptionToBeEdited)
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        self.originalException = StoredException()
        self.newException = Exception(from: StoredException())
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.delegate = self
        timePicker.delegate = self
        timePicker.dataSource = self
        configureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = originalException.name
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if hasExceptionChanged() {
            presenter?.updateException(originalException, with: newException)
        }
    }

    private func hasExceptionChanged() -> Bool {
        let previousException = Exception(from: originalException)

        if newException.date != previousException.date {return true}
        if newException.name != previousException.name {return true}
        if newException.optional != previousException.optional {return true}
        if newException.workTime != previousException.workTime {return true}

        return false
    }
}

extension EditExceptionViewController {

    private func configureView() {
        titleLabel.text = originalException.name
        setTextsInLanguage()
        setDateButton(with: originalException.date!)
        setWorkTimeButton(with: TimeInterval(integerLiteral: originalException.workTime))
        setSwitch()
        datePicker.isHidden = true
        timePicker.isHidden = true
    }

    private func setTextsInLanguage() {
        titleLabel.placeholder = NSLocalizedString("editException.placeholder", comment: "")
        dateLabel.text = NSLocalizedString("addException.date", comment: "")
        workTimeLabel.text = NSLocalizedString("addException.workTime", comment: "")
        infoLabel.text = NSLocalizedString("addException.infoLabel", comment: "")
        optionalLabel.text = NSLocalizedString("addException.optionalLabel", comment: "")
    }

    private func setSwitch() {
        optionalSwitch.isOn = originalException.optional
    }
}

extension EditExceptionViewController {

    @IBAction func dateButtonDidTouchUpInside(_ sender: Any) {
        tabBarController?.tabBar.isHidden = true
        titleLabel.resignFirstResponder()

        datePicker.minimumDate = Date()
        datePicker.setDate(newException.date, animated: true)
        datePicker.isHidden = false
        timePicker.isHidden = true
    }

    @IBAction func workTimeButtonDidTouchUpInside(_ sender: Any) {
        tabBarController?.tabBar.isHidden = true
        titleLabel.resignFirstResponder()

        setTimePickerFrom(timeInterval: newException.workTime)
        datePicker.isHidden = true
        timePicker.isHidden = false
    }

    @IBAction func datePickerDidChangeValue(_ sender: UIDatePicker) {
        newException.date = sender.date.startOfDay
        updateDateButtonFromPicker()

    }

    private func updateDateButtonFromPicker() {
        setDateButton(with: datePicker.date.startOfDay)
    }

    private func setDateButton(with date: Date) {
        let localizedDate = DateFormatter.localizedString(from: date, dateStyle: .full, timeStyle: .none)
        dateButton.setTitle(localizedDate, for: .normal)
    }

    private func updateWorkTimeButtonFromPicker() {
        setWorkTimeButton(with: timeFromTimePicker())
    }

    private func setWorkTimeButton(with workTime: TimeInterval) {

        let localizedTimeInterval = workTime.hourMinuteLocalizedString(style: .full)
        workTimeButton.setTitle(localizedTimeInterval, for: .normal)
    }

    @IBAction func optionalSwitchDidChangeValue(_ sender: Any) {
        tabBarController?.tabBar.isHidden = false
        titleLabel.resignFirstResponder()
        datePicker.isHidden = true
        timePicker.isHidden = true

        newException.optional = optionalSwitch.isOn
    }

    @IBAction func backgroundDidTouchUpInside(_ sender: Any) {
        tabBarController?.tabBar.isHidden = false
        datePicker.isHidden = true
        timePicker.isHidden = true
        titleLabel.resignFirstResponder()
    }
}

extension EditExceptionViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 { //hours
            return 200
        } else { //minutes
            return 60 / 5
        }
    }

    func pickerView(_ pickerView: UIPickerView,
                    attributedTitleForRow row: Int,
                    forComponent component: Int) -> NSAttributedString? {
        if component == 0 { //hours
            return attributedHoursTitleStringFor(row: row)
        } else { // minutes
            return attributedMinutesTitleStringFor(row: row)
        }
    }

    func attributedHoursTitleStringFor(row: Int) -> NSAttributedString {
        let time = TimeInterval(row * 60 * 60)

        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour]
        formatter.unitsStyle = .full

        let title = formatter.string(from: time)!

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .right
        paragraphStyle.tailIndent = -20

        return NSAttributedString(string: title,
                                  attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
    }

    func attributedMinutesTitleStringFor(row: Int) -> NSAttributedString {
        let time = TimeInterval(row * 60 * 5) //5 minute intervals

        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute]
        formatter.unitsStyle = .full

        let title = formatter.string(from: time)!

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left

        return NSAttributedString(string: title,
                                  attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        newException.workTime = timeFromTimePicker()
        updateWorkTimeButtonFromPicker()
    }

    private func setTimePickerFrom(timeInterval: TimeInterval) {
        let totalMinutes = Int(timeInterval / 60)
        let minutes = totalMinutes % 60 / 5
        let hours = (totalMinutes - minutes) / 60
        timePicker.selectRow(hours, inComponent: 0, animated: true)
        timePicker.selectRow(minutes, inComponent: 1, animated: true)
    }

    private func timeFromTimePicker() -> TimeInterval {
        let hours = timePicker.selectedRow(inComponent: 0)
        let minutes = timePicker.selectedRow(inComponent: 1) * 5
        let totalMinutes = hours * 60 + minutes
        let totalSeconds = totalMinutes * 60
        return TimeInterval(totalSeconds)
    }
}

extension EditExceptionViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        datePicker.isHidden = true
        timePicker.isHidden = true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        newException.name = textField.text!
        self.title = textField.text

        return true
    }

    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let textBeforeChange = textField.text as NSString?
        let textAfterChange = textBeforeChange?.replacingCharacters(in: range, with: string)
        newException.name = textAfterChange!
        self.title = textAfterChange

        return true
    }
}

extension EditExceptionViewController: EditExceptionViewControllerProtocol {

}
