//
//  ExceptionsPresenter.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class ExceptionsPresenter {

    weak private var view: ExceptionsViewControllerProtocol?
    private let router: ExceptionsRouterProtocol
    private let interactor: ExceptionsInteractorProtocol

    init(view: ExceptionsViewControllerProtocol,
         router: ExceptionsRouterProtocol,
         interactor: ExceptionsInteractorProtocol) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
}

extension ExceptionsPresenter: ExceptionsPresenterProtocol {

    func addExceptionButtonPressed() {
        router.presentAddExceptionViewController(from: view, presenter: self)
    }

    func cancelButtonPressed() {
        router.popViewController(from: view?.navigationController)
    }

    func saveNewException(newException: Exception) {
        interactor.saveException(newException: newException)
    }

    func retrieveExceptions() {
        interactor.retrieveExceptions()
    }

    func didSelectException(_ exception: StoredException) {
        router.presentEditExceptionViewController(from: view, presenter: self, exceptionToBeEdited: exception)
    }

    func updateException(_ exception: StoredException, with changes: Exception) {
        interactor.updateException(exception, with: changes)
    }

    func deleteException(_ exception: StoredException) {
        interactor.deleteException(exception)
    }
}

extension ExceptionsPresenter: ExceptionsInteractorOutputProtocol {

    func saveExceptionDidSuccess() {
        router.popViewController(from: view?.navigationController)
    }

    func saveExceptionDidFail() {

    }

    func retrieveExceptionsDidSuccess(retrievedExceptions: [StoredException]) {
        view?.reloadTable(exceptions: retrievedExceptions)
    }

    func retrieveExceptionsDidFail() {

    }

    func updateExceptionDidSuccess() {

    }

    func updateExceptionDidFail() {

    }

    func deleteExceptionDidSuccess(deletedException exception: StoredException) {
        view?.removeException(exception: exception)
    }

    func deleteExceptionDidFail() {

    }
}
