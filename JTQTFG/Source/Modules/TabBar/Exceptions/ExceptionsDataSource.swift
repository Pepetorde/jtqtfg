//
//  ExceptionsDataSource.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 08/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class ExceptionsDataSource: ExceptionsDataSourceProtocol {

    var interactor: ExceptionsDataSourceOutputProtocol?

    weak var appDelegate: AppDelegate?
    let managedContext: NSManagedObjectContext?

    init() {
        self.appDelegate = UIApplication.shared.delegate as? AppDelegate
        self.managedContext = appDelegate?.persistentContainer.viewContext
    }
}

extension ExceptionsDataSource {

    func saveException(newException: Exception) {
        let exception = StoredException(entity: StoredException.entity(), insertInto: managedContext)

        exception.name = newException.name
        exception.date = newException.date
        exception.workTime = newException.workTime
        exception.optional = newException.optional

        do {
            try managedContext?.save()
            UserDefaults.standard.set(true, forKey: "scenarioHasChanged")
            interactor?.saveExceptionDidSuccess()
        } catch let error as NSError {
            print("No ha sido posible guardar \(error), \(error.userInfo)")
            interactor?.saveExceptionDidFail()
        }
    }

    func retrieveExceptions() {
        let fetchRequest: NSFetchRequest<StoredException> = StoredException.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]

        do {
            let results = try managedContext?.fetch(fetchRequest)
            interactor?.retrieveExceptionsDidSuccess(retrievedExceptions: results ?? [StoredException]())
        } catch let error as NSError {
            print("No ha sido posible cargar \(error), \(error.userInfo)")
            interactor?.retrieveExceptionsDidFail()
        }
    }

    func updateException(_ exception: StoredException, with updatedException: Exception) {

        exception.name = updatedException.name
        exception.date = updatedException.date
        exception.workTime = updatedException.workTime
        exception.optional = updatedException.optional

        do {
            try managedContext?.save()
            UserDefaults.standard.set(true, forKey: "scenarioHasChanged")
            self.interactor?.updateExceptionDidSuccess()
        } catch let error as NSError {
            print("No ha sido posible actualizar \(error), \(error.userInfo)")
            interactor?.updateExceptionDidFail()
        }
    }

    func deleteException(_ exception: StoredException) {
        managedContext?.delete(exception)

        do {
            try managedContext?.save()
            UserDefaults.standard.set(true, forKey: "scenarioHasChanged")
            interactor?.deleteExceptionDidSuccess(deletedException: exception)
        } catch let error as NSError {
            print("No ha sido posible eliminar \(error), \(error.userInfo)")
            interactor?.deleteExceptionDidFail()
        }
    }
}

struct Exception {
    var name: String
    var date: Date
    var workTime: TimeInterval
    var optional: Bool

    init(from exception: StoredException) {
        name = exception.name!
        date = exception.date!
        workTime = exception.workTime
        optional = exception.optional
    }

    init() {
        name = ""
        date = Date.today()
        workTime = 0
        optional = false
    }
}
