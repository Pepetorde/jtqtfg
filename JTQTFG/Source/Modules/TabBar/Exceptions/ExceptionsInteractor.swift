//
//  ExceptionsInteractor.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 08/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import CoreData

class ExceptionsInteractor: ExceptionsInteractorProtocol {

    var presenter: ExceptionsInteractorOutputProtocol?
    private let dataSource: ExceptionsDataSourceProtocol

    init(dataSource: ExceptionsDataSourceProtocol) {
        self.dataSource = dataSource
    }
}

extension ExceptionsInteractor {
    func saveException(newException: Exception) {
        dataSource.saveException(newException: newException)
    }

    func retrieveExceptions() {
        dataSource.retrieveExceptions()
    }

    func updateException(_ exception: StoredException, with changes: Exception) {
        dataSource.updateException(exception, with: changes)
    }

    func deleteException(_ exception: StoredException) {
        dataSource.deleteException(exception)
    }
}

extension ExceptionsInteractor: ExceptionsDataSourceOutputProtocol {

    func saveExceptionDidSuccess() {
        presenter?.saveExceptionDidSuccess()
    }

    func saveExceptionDidFail() {
        presenter?.saveExceptionDidFail()
    }

    func retrieveExceptionsDidSuccess(retrievedExceptions: [StoredException]) {
        presenter?.retrieveExceptionsDidSuccess(retrievedExceptions: retrievedExceptions)
    }

    func retrieveExceptionsDidFail() {
        presenter?.retrieveExceptionsDidFail()
    }

    func updateExceptionDidSuccess() {
        presenter?.updateExceptionDidSuccess()
    }

    func updateExceptionDidFail() {
        presenter?.updateExceptionDidFail()
    }

    func deleteExceptionDidSuccess(deletedException exception: StoredException) {
        presenter?.deleteExceptionDidSuccess(deletedException: exception)
    }

    func deleteExceptionDidFail() {
        presenter?.deleteExceptionDidFail()
    }
}
