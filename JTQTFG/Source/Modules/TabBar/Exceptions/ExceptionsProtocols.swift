//
//  ExceptionsProtocols.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit

protocol ExceptionsRouterProtocol: class {
    func presentAddExceptionViewController(from previousViewController: UIViewController?,
                                           presenter: ExceptionsPresenterProtocol)
    func presentEditExceptionViewController(from previousViewController: UIViewController?,
                                            presenter: ExceptionsPresenterProtocol,
                                            exceptionToBeEdited: StoredException)
    func popViewController(from navigationController: UINavigationController?)
}

protocol ExceptionsPresenterProtocol: class {
    func addExceptionButtonPressed()
    func cancelButtonPressed()
    func saveNewException(newException: Exception)
    func retrieveExceptions()
    func didSelectException(_ exception: StoredException)
    func updateException(_ exception: StoredException, with changes: Exception)
    func deleteException(_ exception: StoredException)
}

protocol ExceptionsInteractorProtocol: class {
    func saveException(newException: Exception)
    func retrieveExceptions()
    func updateException(_ exception: StoredException, with changes: Exception)
    func deleteException(_ exception: StoredException)
}

protocol ExceptionsInteractorOutputProtocol: class {
    func saveExceptionDidSuccess()
    func saveExceptionDidFail()
    func retrieveExceptionsDidSuccess(retrievedExceptions: [StoredException])
    func retrieveExceptionsDidFail()
    func updateExceptionDidSuccess()
    func updateExceptionDidFail()
    func deleteExceptionDidSuccess(deletedException exception: StoredException)
    func deleteExceptionDidFail()
}

protocol ExceptionsDataSourceProtocol: class {
    func saveException(newException: Exception)
    func retrieveExceptions()
    func updateException(_ exception: StoredException, with updatedException: Exception)
    func deleteException(_ exception: StoredException)
}

protocol ExceptionsDataSourceOutputProtocol: class {
    func saveExceptionDidSuccess()
    func saveExceptionDidFail()
    func retrieveExceptionsDidSuccess(retrievedExceptions: [StoredException])
    func retrieveExceptionsDidFail()
    func updateExceptionDidSuccess()
    func updateExceptionDidFail()
    func deleteExceptionDidSuccess(deletedException exception: StoredException)
    func deleteExceptionDidFail()
}

protocol ExceptionsViewControllerProtocol: UIViewController {
    func reloadTable(exceptions: [StoredException])
    func removeException(exception: StoredException)
}

protocol AddExceptionViewControllerProtocol: class {

}

protocol EditExceptionViewControllerProtocol: class {

}
