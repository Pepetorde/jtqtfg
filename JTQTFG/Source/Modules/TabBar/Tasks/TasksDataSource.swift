//
//  TasksDataSource.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 25/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class TasksDataSource: TasksDataSourceProtocol {

    var interactor: TasksDataSourceOutputProtocol?

    weak var appDelegate: AppDelegate?
    let managedContext: NSManagedObjectContext?

    init() {
        self.appDelegate = UIApplication.shared.delegate as? AppDelegate
        self.managedContext = appDelegate?.persistentContainer.viewContext
    }
}

extension TasksDataSource {

    func saveTask(newTask: Task) {
        let task = StoredTask(entity: StoredTask.entity(), insertInto: managedContext)

        task.name = newTask.name
        task.startDate = newTask.startDate
        task.deadline = newTask.deadline
        task.workTime = newTask.workTime
        task.completion = newTask.completion.rawValue
        task.minimumTime = newTask.minimumTime
        task.contextChange = newTask.contextChange
        task.overlap = newTask.overlap
        task.optional = newTask.optional

        do {
            try managedContext?.save()
            UserDefaults.standard.set(true, forKey: "scenarioHasChanged")
            interactor?.saveTaskDidSuccess()
        } catch let error as NSError {
            print("No ha sido posible guardar \(error), \(error.userInfo)")
            interactor?.saveTaskDidFail()
        }
    }

    func retrieveTasks() {
        let fetchRequest: NSFetchRequest<StoredTask> = StoredTask.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "deadline", ascending: true)]

        do {
            let results = try managedContext?.fetch(fetchRequest)
            interactor?.retrieveTasksDidSuccess(retrievedTasks: results ?? [StoredTask]())
        } catch let error as NSError {
            print("No ha sido posible cargar \(error), \(error.userInfo)")
            interactor?.retrieveTasksDidFail()
        }
    }

    func updateTask(_ task: StoredTask, with updatedTask: Task) {

        task.name = updatedTask.name
        task.startDate = updatedTask.startDate
        task.deadline = updatedTask.deadline
        task.workTime = updatedTask.workTime
        task.completion = updatedTask.completion.rawValue
        task.minimumTime = updatedTask.minimumTime
        task.contextChange = updatedTask.contextChange
        task.overlap = updatedTask.overlap
        task.optional = updatedTask.optional

        do {
            try managedContext?.save()
            UserDefaults.standard.set(true, forKey: "scenarioHasChanged")
            self.interactor?.updateTaskDidSuccess()
        } catch let error as NSError {
            print("No ha sido posible actualizar \(error), \(error.userInfo)")
            interactor?.updateTaskDidFail()
        }
    }

    func deleteTask(_ task: StoredTask) {
        managedContext?.delete(task)

        do {
            try managedContext?.save()
            UserDefaults.standard.set(true, forKey: "scenarioHasChanged")
            interactor?.deleteTaskDidSuccess(deletedTask: task)
        } catch let error as NSError {
            print("No ha sido posible eliminar \(error), \(error.userInfo)")
            interactor?.deleteTaskDidFail()
        }
    }
}

struct Task {
    var name: String
    var startDate: Date
    var deadline: Date
    var workTime: TimeInterval
    var completion: Completion
    var minimumTime: TimeInterval
    var contextChange: TimeInterval
    var overlap: Bool
    var optional: Bool

    var workWindowDays: Int {
        let components = Calendar.current.dateComponents([.day], from: startDate.startOfDay, to: deadline.startOfDay)
        return (components.day ?? 0) + 1
    }

    init(from task: StoredTask) {
        name = task.name!
        startDate = task.startDate!
        deadline = task.deadline!
        workTime = task.workTime
        completion = Completion(rawValue: task.completion)!
        minimumTime = task.minimumTime
        contextChange = task.contextChange
        overlap = task.overlap
        optional = task.optional
    }

    init() {
        name = ""
        startDate = Date.today().startOfDay
        deadline = Date.today().startOfDay
        workTime = 0
        completion = Completion.whenever
        minimumTime = 0
        contextChange = 0
        overlap = true
        optional = false
    }

    static func == (lhs: Task, rhs: Task) -> Bool {
        if lhs.name == rhs.name && lhs.startDate == rhs.startDate && lhs.deadline == rhs.deadline {
            return true
        } else {
            return false
        }
    }
}

enum Completion: Int32 {
    case whenever, asap, alap
}
