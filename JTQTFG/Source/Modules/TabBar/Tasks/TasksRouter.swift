//
//  TasksRouter.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import UIKit

class TasksRouter: NestedTabBarViewRouterProtocol {

    var tabIcon: UIImage = UIImage(named: "check-list")!
    var tabTitle: String = NSLocalizedString("tasks.tabTitle", comment: "")

    let mainRouter: MainRouterInterface?

    init(mainRouter: MainRouterInterface? = nil) {
        self.mainRouter = mainRouter
    }

    static func createModule(mainRouter: MainRouter? = nil) -> UIViewController {
        let view = TasksViewController()
        let router = TasksRouter(mainRouter: mainRouter)
        let dataSource = TasksDataSource()
        let interactor = TasksInteractor(dataSource: dataSource)
        let presenter = TasksPresenter(view: view, router: router, interactor: interactor)

        view.presenter = presenter
        interactor.presenter = presenter
        dataSource.interactor = interactor

        return view
    }

    func getAnidatedTabBarModule() -> UIViewController {
        let view = TasksViewController()
        let router = TasksRouter(mainRouter: mainRouter)
        let dataSource = TasksDataSource()
        let interactor = TasksInteractor(dataSource: dataSource)
        let presenter = TasksPresenter(view: view, router: router, interactor: interactor)

        view.presenter = presenter
        interactor.presenter = presenter
        dataSource.interactor = interactor

        return view
    }
}

extension TasksRouter: TasksRouterProtocol {

    func presentAddTaskViewController(from previousViewController: UIViewController?,
                                      presenter: TasksPresenterProtocol) {
        let addTaskViewController = AddTaskViewController()
        addTaskViewController.presenter = presenter
        previousViewController?.navigationController?.pushViewController(addTaskViewController, animated: true)
    }

    func presentEditTaskViewController(from previousViewController: UIViewController?,
                                       presenter: TasksPresenterProtocol,
                                       taskToBeEdited: StoredTask) {
        let editTaskViewController = EditTaskViewController(taskToBeEdited: taskToBeEdited)
        editTaskViewController.presenter = presenter
        previousViewController?.navigationController?.pushViewController(editTaskViewController, animated: true)
    }

    func popViewController(from navigationController: UINavigationController?) {
        navigationController?.popViewController(animated: true)
    }
}
