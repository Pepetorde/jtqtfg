//
//  TasksProtocols.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol TasksRouterProtocol: class {
    func presentAddTaskViewController(from previousViewController: UIViewController?, presenter: TasksPresenterProtocol)
    func presentEditTaskViewController(from previousViewController: UIViewController?,
                                       presenter: TasksPresenterProtocol,
                                       taskToBeEdited: StoredTask)
    func popViewController(from navigationController: UINavigationController?)
}

protocol TasksPresenterProtocol: class {
    func addTaskButtonPressed()
    func cancelButtonPressed()
    func saveNewTask(newTask: Task)
    func retrieveTasks()
    func didSelectTask(_ task: StoredTask)
    func updateTask(_ task: StoredTask, with changes: Task)
    func deleteTask(_ task: StoredTask)
}

protocol TasksInteractorProtocol: class {
    func saveTask(newTask: Task)
    func retrieveTasks()
    func updateTask(_ task: StoredTask, with changes: Task)
    func deleteTask(_ task: StoredTask)
}

protocol TasksInteractorOutputProtocol: class {
    func saveTaskDidSuccess()
    func saveTaskDidFail()
    func retrieveTasksDidSuccess(retrievedTasks: [StoredTask])
    func retrieveTasksDidFail()
    func updateTaskDidSuccess()
    func updateTaskDidFail()
    func deleteTaskDidSuccess(deletedTask task: StoredTask)
    func deleteTaskDidFail()
}

protocol TasksDataSourceProtocol: class {
    func saveTask(newTask: Task)
    func retrieveTasks()
    func updateTask(_ task: StoredTask, with updatedTask: Task)
    func deleteTask(_ task: StoredTask)
}

protocol TasksDataSourceOutputProtocol: class {
    func saveTaskDidSuccess()
    func saveTaskDidFail()
    func retrieveTasksDidSuccess(retrievedTasks: [StoredTask])
    func retrieveTasksDidFail()
    func updateTaskDidSuccess()
    func updateTaskDidFail()
    func deleteTaskDidSuccess(deletedTask task: StoredTask)
    func deleteTaskDidFail()
}

protocol TasksViewControllerProtocol: UIViewController {
    func reloadTable(tasks: [StoredTask])
    func removeTask(task: StoredTask)
}

protocol AddTaskViewControllerProtocol: class {

}

protocol EditTaskViewControllerProtocol: class {

}
