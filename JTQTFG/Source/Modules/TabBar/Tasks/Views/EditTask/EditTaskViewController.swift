//
//  EditTaskViewController.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 21/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import UIKit

class EditTaskViewController: ParentViewController {

    @IBOutlet weak var titleLabel: UITextField!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var deadlineLabel: UILabel!
    @IBOutlet weak var deadlineInfoLabel: UILabel!
    @IBOutlet weak var workTimeLabel: UILabel!
    @IBOutlet weak var workTimeInfoLabel: UILabel!
    @IBOutlet weak var completionLabel: UILabel!
    @IBOutlet weak var minimumTimeLabel: UILabel!
    @IBOutlet weak var contextChangeLabel: UILabel!
    @IBOutlet weak var contextChangeInfoLabel: UILabel!
    @IBOutlet weak var overlapLabel: UILabel!
    @IBOutlet weak var overlapInfoLabel: UILabel!
    @IBOutlet weak var optionalLabel: UILabel!

    @IBOutlet weak var startDateButton: UIButton!
    @IBOutlet weak var deadlineButton: UIButton!
    @IBOutlet weak var workTimeButton: UIButton!
    @IBOutlet weak var completionButton: UIButton!
    @IBOutlet weak var minimumTimeButton: UIButton!
    @IBOutlet weak var contextChangeButton: UIButton!

    @IBOutlet weak var overlapSwitch: UISwitch!
    @IBOutlet weak var optionalSwitch: UISwitch!

    @IBOutlet weak var completionPicker: UIPickerView!

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var timePicker: UIPickerView!
    var completionPickerValues = [String]()

    @IBOutlet weak var bottomScrollConstraint: NSLayoutConstraint!
    var isBottomScrollViewMarginSetToHigh = false

    var presenter: TasksPresenterProtocol?

    var originalTask: StoredTask
    var newTask: Task

    var lastDateButtonPressed: UIButton?
    var lastTimeButtonPressed: UIButton?

    init(taskToBeEdited: StoredTask) {
        self.originalTask = taskToBeEdited
        self.newTask = Task(from: taskToBeEdited)
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        self.originalTask = StoredTask()
        self.newTask = Task(from: StoredTask())
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        titleLabel.delegate = self
        completionPicker.delegate = self
        completionPicker.dataSource = self
        timePicker.delegate = self
        timePicker.dataSource = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = originalTask.name
        hideAll()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if hasTaskChanged() {
            presenter?.updateTask(originalTask, with: newTask)
        }
    }
}

extension EditTaskViewController {
    private func configureView() {
        titleLabel.text = originalTask.name
        setTextsInLanguage()
        setDatePicker()
        setSwitches()
        setButtonsText()
    }

    private func setTextsInLanguage() {
        titleLabel.placeholder = NSLocalizedString("editTask.placeholder", comment: "")
        startDateLabel.text = NSLocalizedString("addTask.startDate", comment: "")
        deadlineLabel.text = NSLocalizedString("addTask.deadline", comment: "")
        workTimeLabel.text = NSLocalizedString("addTask.workTime", comment: "")
        workTimeInfoLabel.text = NSLocalizedString("addTask.workTimeInfo", comment: "")
        deadlineInfoLabel.text = NSLocalizedString("addTask.deadlineInfo", comment: "")
        completionLabel.text = NSLocalizedString("addTask.completion", comment: "")
        completionPickerValues.append(NSLocalizedString("addTask.completion.whenPossible", comment: ""))
        completionPickerValues.append(NSLocalizedString("addTask.completion.asSoon", comment: ""))
        completionPickerValues.append(NSLocalizedString("addTask.completion.asLate", comment: ""))
        minimumTimeLabel.text = NSLocalizedString("addTask.minimumTime", comment: "")
        contextChangeLabel.text = NSLocalizedString("addTask.contextChange", comment: "")
        contextChangeInfoLabel.text = NSLocalizedString("addTask.contextChangeInfo", comment: "")
        overlapLabel.text = NSLocalizedString("addTask.overlap", comment: "")
        overlapInfoLabel.text = NSLocalizedString("addTask.overlapInfo", comment: "")
        optionalLabel.text = NSLocalizedString( "addTask.optional", comment: "")
    }

    private func setDatePicker() {
        datePicker.datePickerMode = .date
    }

    private func setSwitches() {
        overlapSwitch.isOn = !originalTask.overlap
        optionalSwitch.isOn = originalTask.optional
    }

    private func setButtonsText() {
        updateStartDateButtonText()
        updateDeadlineButtonText()
        updateWorkTimeButtonText()
        updateCompletionButtonText()
        updateMinimumTimeButtonText()
        updateContextChangeButtonText()
    }

    private func hasTaskChanged() -> Bool {
        let previousTask = Task(from: originalTask)

        if newTask.completion != previousTask.completion {return true}
        if newTask.contextChange != previousTask.contextChange {return true}
        if newTask.deadline != previousTask.deadline {return true}
        if newTask.minimumTime != previousTask.minimumTime {return true}
        if newTask.name != previousTask.name {return true}
        if newTask.optional != previousTask.optional {return true}
        if newTask.overlap != previousTask.overlap {return true}
        if newTask.startDate != previousTask.startDate {return true}
        if newTask.workTime != previousTask.workTime {return true}

        return false
    }
}

extension EditTaskViewController: EditTaskViewControllerProtocol {

}
