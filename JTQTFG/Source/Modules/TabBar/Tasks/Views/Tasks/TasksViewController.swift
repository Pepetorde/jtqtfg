//
//  TasksViewController.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import UIKit

class TasksViewController: ParentViewController {

    @IBOutlet weak var tasksTable: UITableView!

    var presenter: TasksPresenterProtocol?

    var tasks = [StoredTask]()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.title = NSLocalizedString("tasks.navTitle", comment: "")
        presenter?.retrieveTasks()
    }
}

extension TasksViewController {

    private func configureView() {
        setNavigationBar()
        configureTableView()
    }

    private func setNavigationBar() {
        let addTaskButton = UIBarButtonItem(
            barButtonSystemItem: .add,
            target: self,
            action: #selector(addTaskButtonDidTouchUpInside))
        navigationItem.rightBarButtonItem = addTaskButton
    }

    private func configureTableView() {
        tasksTable.dataSource = self
        tasksTable.delegate = self
        let taskCellNib = UINib(nibName: "TasksTableCell", bundle: nil)
        tasksTable.register(taskCellNib, forCellReuseIdentifier: "taskTableCell")
    }
}

extension TasksViewController {
    @objc private func addTaskButtonDidTouchUpInside() {
        presenter?.addTaskButtonPressed()
    }
}

extension TasksViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tasksTable.dequeueReusableCell(withIdentifier: "taskTableCell") as? TaskTableCell {

            let task = tasks[indexPath.row]

            let taskName = task.name

            let taskIsPast = task.deadline! < Date().startOfDay

            if task.workTime == 0 || taskIsPast {
                let attributedString = NSMutableAttributedString(string: taskName ?? "")
                let range = NSRange(location: 0, length: attributedString.length)
                attributedString.addAttribute(.strikethroughStyle, value: 2, range: range)
                cell.taskName.attributedText = attributedString
                cell.taskName.textColor = .lightGray
            } else {
                cell.taskName.attributedText = nil
                cell.taskName.textColor = .darkText
                cell.taskName.text = taskName
            }

            cell.taskDeadline.text = task.deadline!.longLocalizedString()
            cell.taskWorkTime.text = TimeInterval(task.workTime).hourMinuteLocalizedString(style: .short)
            return cell
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedTask = tasks[indexPath.row]
        presenter?.didSelectTask(selectedTask)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView,
                   commit editingStyle: UITableViewCell.EditingStyle,
                   forRowAt indexPath: IndexPath) {
        presenter?.deleteTask(tasks[indexPath.row])
    }
}

extension TasksViewController: TasksViewControllerProtocol {
    func reloadTable(tasks: [StoredTask]) {
        self.tasks = tasks
        tasksTable.reloadData()
    }

    func removeTask(task: StoredTask) {
        if let index = tasks.firstIndex(of: task) {
            tasks.remove(at: index)
            tasksTable.beginUpdates()
            tasksTable.deleteRows(at: [IndexPath(row: index, section: 0)], with: .left)
            tasksTable.endUpdates()
        }
    }
}

class TaskTableCell: UITableViewCell {

    @IBOutlet weak var taskName: UILabel!
    @IBOutlet weak var taskWorkTime: UILabel!
    @IBOutlet weak var taskDeadline: UILabel!

}
