//
//  AddTaskViewController.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 21/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import UIKit

class AddTaskViewController: ParentViewController {

    @IBOutlet weak var titleLabel: UITextField!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var deadlineLabel: UILabel!
    @IBOutlet weak var deadlineInfoLabel: UILabel!
    @IBOutlet weak var workTimeLabel: UILabel!
    @IBOutlet weak var workTimeInfoLabel: UILabel!
    @IBOutlet weak var completionLabel: UILabel!
    @IBOutlet weak var minimumTimeLabel: UILabel!
    @IBOutlet weak var contextChangeLabel: UILabel!
    @IBOutlet weak var contextChangeInfoLabel: UILabel!
    @IBOutlet weak var overlapLabel: UILabel!
    @IBOutlet weak var overlapInfoLabel: UILabel!
    @IBOutlet weak var optionalLabel: UILabel!

    @IBOutlet weak var startDateButton: UIButton!
    @IBOutlet weak var deadlineButton: UIButton!
    @IBOutlet weak var workTimeButton: UIButton!
    @IBOutlet weak var completionButton: UIButton!
    @IBOutlet weak var minimumTimeButton: UIButton!
    @IBOutlet weak var contextChangeButton: UIButton!

    @IBOutlet weak var overlapSwitch: UISwitch!
    @IBOutlet weak var optionalSwitch: UISwitch!

    @IBOutlet weak var completionPicker: UIPickerView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var timePicker: UIPickerView!
    var completionPickerValues = [String]()

    @IBOutlet weak var bottomScrollConstraint: NSLayoutConstraint!
    var isBottomScrollViewMarginSetToHigh = false

    var presenter: TasksPresenterProtocol?

    var newTask = Task()

    var lastDateButtonPressed: UIButton?
    var lastTimeButtonPressed: UIButton?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        titleLabel.delegate = self
        completionPicker.delegate = self
        completionPicker.dataSource = self
        timePicker.delegate = self
        timePicker.dataSource = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = NSLocalizedString("addTask.navTitle", comment: "")
        titleLabel.becomeFirstResponder()
        updateCompletionButtonText()
        updateContextChangeButtonText()
        updateMinimumTimeButtonText()
    }
}

extension AddTaskViewController {
    private func configureView() {
        setNavigationBar()
        setTextsInLanguage()
        datePicker.datePickerMode = .date
        setButtonsText()
    }

    private func setNavigationBar() {
        let backButton = UIBarButtonItem(barButtonSystemItem: .cancel,
                                         target: self,
                                         action: #selector(cancelButtonDidTouchUpInside))
        let addTaskButton = UIBarButtonItem(barButtonSystemItem: .done,
                                            target: self,
                                            action: #selector(doneButtonDidTouchUpInside))

        navigationItem.rightBarButtonItem = addTaskButton
        navigationItem.leftBarButtonItem = backButton
    }

    private func setTextsInLanguage() {
        titleLabel.placeholder = NSLocalizedString("addTask.placeholder", comment: "")
        startDateLabel.text = NSLocalizedString("addTask.startDate", comment: "")
        startDateButton.setTitle(NSLocalizedString("addTask.startDateButton", comment: ""), for: .normal)
        deadlineLabel.text = NSLocalizedString("addTask.deadline", comment: "")
        deadlineButton.setTitle(NSLocalizedString("addTask.dateButton", comment: ""), for: .normal)
        workTimeButton.setTitle(NSLocalizedString("addTask.workTimeButton", comment: ""), for: .normal)
        workTimeLabel.text = NSLocalizedString("addTask.workTime", comment: "")
        workTimeInfoLabel.text = NSLocalizedString("addTask.workTimeInfo", comment: "")
        deadlineInfoLabel.text = NSLocalizedString("addTask.deadlineInfo", comment: "")
        completionLabel.text = NSLocalizedString("addTask.completion", comment: "")
        minimumTimeLabel.text = NSLocalizedString("addTask.minimumTime", comment: "")
        contextChangeLabel.text = NSLocalizedString("addTask.contextChange", comment: "")
        contextChangeInfoLabel.text = NSLocalizedString("addTask.contextChangeInfo", comment: "")
        overlapLabel.text = NSLocalizedString("addTask.overlap", comment: "")
        overlapInfoLabel.text = NSLocalizedString("addTask.overlapInfo", comment: "")
        optionalLabel.text = NSLocalizedString( "addTask.optional", comment: "")

        completionPickerValues.append(NSLocalizedString("addTask.completion.whenPossible", comment: ""))
        completionPickerValues.append(NSLocalizedString("addTask.completion.asSoon", comment: ""))
        completionPickerValues.append(NSLocalizedString("addTask.completion.asLate", comment: ""))
    }

    private func setButtonsText() {
        updateStartDateButtonText()
        updateDeadlineButtonText()
    }
}

extension AddTaskViewController: AddTaskViewControllerProtocol {

}
