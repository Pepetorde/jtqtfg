//
//  AddTaskViewControllerComplements.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 13/03/2020.
//  Copyright © 2020 José Tordesillas Quintana. All rights reserved.
//

import UIKit

extension AddTaskViewController {

    @objc internal func cancelButtonDidTouchUpInside() {
        tabBarController?.tabBar.isHidden = false
        presenter?.cancelButtonPressed()
    }

    @objc internal func doneButtonDidTouchUpInside() {
        var fieldsAreCorrectlyFilled = true

        if newTask.name == "" {
            titleLabel.errorWiggle()
            fieldsAreCorrectlyFilled = false
        }

        if newTask.workTime == 0 {
            workTimeButton.errorWiggle()
            fieldsAreCorrectlyFilled = false
        }

        if newTask.startDate > newTask.deadline {
            startDateButton.errorWiggle()
            deadlineButton.errorWiggle()
            fieldsAreCorrectlyFilled = false
        }

        if fieldsAreCorrectlyFilled {
            tabBarController?.tabBar.isHidden = false
            presenter?.saveNewTask(newTask: newTask)
        }
    }

    @IBAction func startDateDidTouchUpInside(_ sender: Any) {
        lastDateButtonPressed = startDateButton
        newTask.startDate = newTask.startDate
        updateStartDateButtonText()

        datePicker.minimumDate = nil
        datePicker.maximumDate = nil
        datePicker.setDate(newTask.startDate, animated: true)
        showDatePicker()
    }

    @IBAction func deadlineButtonDidTouchUpInside(_ sender: Any) {
        lastDateButtonPressed = deadlineButton
        newTask.deadline = newTask.deadline
        updateDeadlineButtonText()

        datePicker.minimumDate = nil
        datePicker.maximumDate = nil
        datePicker.setDate(newTask.deadline, animated: true)
        showDatePicker()
    }

    @IBAction func workTimeButtonDidTouchUpInside(_ sender: Any) {
        lastTimeButtonPressed = workTimeButton
        newTask.workTime = newTask.workTime
        updateWorkTimeButtonText()
        setTimePickerFrom(timeInterval: newTask.workTime)
        showTimePicker()
    }

    @IBAction func completionButtonDidTouchUpInside(_ sender: Any) {
        showCompletionPicker()
    }

    @IBAction func minimumTimeButtonDidTouchUpInside(_ sender: Any) {
        lastTimeButtonPressed = minimumTimeButton
        updateMinimumTimeButtonText()
        setTimePickerFrom(timeInterval: newTask.minimumTime)
        showTimePicker()
    }

    @IBAction func contextChangeButtonDidTouchUpInside(_ sender: Any) {
        lastTimeButtonPressed = contextChangeButton
        updateContextChangeButtonText()
        setTimePickerFrom(timeInterval: newTask.contextChange)
        showTimePicker()
    }

    private func setTimePickerFrom(timeInterval: TimeInterval) {
        let totalMinutes = Int(timeInterval / 60)
        let minutes = totalMinutes % 60 / 5
        let hours = (totalMinutes - minutes) / 60
        timePicker.selectRow(hours, inComponent: 0, animated: true)
        timePicker.selectRow(minutes, inComponent: 1, animated: true)
    }

    @IBAction func overlapSwitchValueDidChange(_ sender: UISwitch) {
        hideAll()
        newTask.overlap = !sender.isOn
    }

    @IBAction func optionalSwitchValueDidChange(_ sender: UISwitch) {
        hideAll()
        newTask.optional = sender.isOn
    }

    @IBAction func datePickerValueDidChange(_ sender: UIDatePicker) {
        if lastDateButtonPressed == startDateButton {
            newTask.startDate = sender.date.startOfDay
            updateStartDateButtonText()
        } else if lastDateButtonPressed == deadlineButton {
            newTask.deadline = sender.date.startOfDay
            updateDeadlineButtonText()
        }
    }

    @IBAction func backgroundButtonDidTouchUpInside(_ sender: Any) {
        hideAll()
    }
}

extension AddTaskViewController {
    private func showDatePicker() {
        tabBarController?.tabBar.isHidden = true
        titleLabel.resignFirstResponder()
        setBottomScrollMarginToHigh()
        datePicker.isHidden = false
        completionPicker.isHidden = true
        timePicker.isHidden = true
    }

    private func showTimePicker() {
        tabBarController?.tabBar.isHidden = true
        titleLabel.resignFirstResponder()
        setBottomScrollMarginToHigh()
        datePicker.isHidden = true
        completionPicker.isHidden = true
        timePicker.isHidden = false
    }

    private func showCompletionPicker() {
        tabBarController?.tabBar.isHidden = true
        titleLabel.resignFirstResponder()
        datePicker.isHidden = true
        completionPicker.isHidden = false
        timePicker.isHidden = true
        setBottomScrollMarginToHigh()
    }

    private func hideAll() {
        tabBarController?.tabBar.isHidden = false
        titleLabel.resignFirstResponder()
        setBottomScrollMarginToLow()
        datePicker.isHidden = true
        completionPicker.isHidden = true
        timePicker.isHidden = true
    }

    private func setBottomScrollMarginToHigh() {
        if isBottomScrollViewMarginSetToHigh == false {
            bottomScrollConstraint.constant = datePicker.bounds.height - 90
        }
        isBottomScrollViewMarginSetToHigh = true
    }

    private func setBottomScrollMarginToLow() {
        if isBottomScrollViewMarginSetToHigh == true {
            bottomScrollConstraint.constant -= datePicker.bounds.height - 90
        }
        isBottomScrollViewMarginSetToHigh = false
    }
}

extension AddTaskViewController {
    internal func updateStartDateButtonText() {
        startDateButton.setTitle(newTask.startDate.fullLocalizedString(), for: .normal)
    }

    internal func updateDeadlineButtonText() {
        deadlineButton.setTitle(newTask.deadline.fullLocalizedString(), for: .normal)
    }

    private func updateWorkTimeButtonText() {
        let title = newTask.workTime.hourMinuteLocalizedString(style: .full)
        workTimeButton.setTitle(title, for: .normal)
    }

    internal func updateCompletionButtonText() {
        let completionIndex = Int(newTask.completion.rawValue)
        completionButton.setTitle(completionPickerValues[completionIndex], for: .normal)
    }

    internal func updateMinimumTimeButtonText() {
        let title = newTask.minimumTime.hourMinuteLocalizedString(style: .full)
        minimumTimeButton.setTitle(title, for: .normal)
    }

    internal func updateContextChangeButtonText() {
        let title = newTask.contextChange.hourMinuteLocalizedString(style: .full)
        contextChangeButton.setTitle(title, for: .normal)
    }
}

extension AddTaskViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        datePicker.isHidden = true
        completionPicker.isHidden = true
        timePicker.isHidden = true
        setBottomScrollMarginToHigh()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        newTask.name = textField.text ?? ""
        textField.resignFirstResponder()
        setBottomScrollMarginToLow()
        return true
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        newTask.name = textField.text ?? ""
        textField.resignFirstResponder()
        setBottomScrollMarginToLow()
        return true
    }

    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let textBeforeChange = textField.text as NSString?
        let textAfterChange = textBeforeChange?.replacingCharacters(in: range, with: string)
        newTask.name = textAfterChange ?? ""
        return true
    }
}

extension AddTaskViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        var numberOfComponents = 0
        if pickerView == completionPicker {
            numberOfComponents = 1
        }
        if pickerView == timePicker {
            numberOfComponents = 2
        }
        return numberOfComponents
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var numberOfRows = 0
        if pickerView == completionPicker {
            numberOfRows = completionPickerValues.count
        }
        if pickerView == timePicker {
            if component == 0 { //hours
                numberOfRows = 200
            } else { //minutes
                numberOfRows = 60 / 5
            }
        }
        return numberOfRows
    }

    func pickerView(_ pickerView: UIPickerView,
                    attributedTitleForRow row: Int,
                    forComponent component: Int) -> NSAttributedString? {

        var attributedTitle = NSAttributedString()

        if pickerView == completionPicker {
            attributedTitle = NSAttributedString(string: completionPickerValues[row])
        }

        if pickerView == timePicker {
            if component == 0 { //hours
                attributedTitle = attributedHoursTitleStringFor(row: row)
            }
            if component == 1 { // minutes
                attributedTitle = attributedMinutesTitleStringFor(row: row)
            }
        }

        return attributedTitle
    }

    func attributedHoursTitleStringFor(row: Int) -> NSAttributedString {
        let time = TimeInterval(row * 60 * 60)

        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour]
        formatter.unitsStyle = .full

        let title = formatter.string(from: time)!

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .right
        paragraphStyle.tailIndent = -20

        return NSAttributedString(string: title,
                                  attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
    }

    func attributedMinutesTitleStringFor(row: Int) -> NSAttributedString {
        let time = TimeInterval(row * 60 * 5) //5 minute intervals

        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute]
        formatter.unitsStyle = .full

        let title = formatter.string(from: time)!

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left

        return NSAttributedString(string: title,
                                  attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == completionPicker {
            newTask.completion = Completion(rawValue: Int32(row)) ?? .whenever
            updateCompletionButtonText()
        }

        if pickerView == timePicker {
            switch lastTimeButtonPressed {
            case workTimeButton:
                newTask.workTime = timeFromTimePicker()
                updateWorkTimeButtonText()
            case minimumTimeButton:
                newTask.minimumTime = timeFromTimePicker()
                updateMinimumTimeButtonText()
            default:
                newTask.contextChange = timeFromTimePicker()
                updateContextChangeButtonText()
            }
        }
    }

    private func timeFromTimePicker() -> TimeInterval {
        let hours = timePicker.selectedRow(inComponent: 0)
        let minutes = timePicker.selectedRow(inComponent: 1) * 5
        let totalMinutes = hours * 60 + minutes
        let totalSeconds = totalMinutes * 60
        return TimeInterval(totalSeconds)
    }
}
