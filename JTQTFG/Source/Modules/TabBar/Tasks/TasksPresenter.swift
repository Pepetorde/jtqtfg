//
//  TasksPresenter.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class TasksPresenter {

    weak private var view: TasksViewControllerProtocol?
    private let router: TasksRouterProtocol
    private let interactor: TasksInteractorProtocol

    init(view: TasksViewControllerProtocol, router: TasksRouterProtocol, interactor: TasksInteractorProtocol) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
}

extension TasksPresenter: TasksPresenterProtocol {

    func addTaskButtonPressed() {
        router.presentAddTaskViewController(from: view, presenter: self)
    }

    func cancelButtonPressed() {
        router.popViewController(from: view?.navigationController)
    }

    func saveNewTask(newTask: Task) {
        interactor.saveTask(newTask: newTask)
    }

    func retrieveTasks() {
        interactor.retrieveTasks()
    }

    func didSelectTask(_ task: StoredTask) {
        router.presentEditTaskViewController(from: view, presenter: self, taskToBeEdited: task)
    }

    func updateTask(_ task: StoredTask, with changes: Task) {
        interactor.updateTask(task, with: changes)
    }

    func deleteTask(_ task: StoredTask) {
        interactor.deleteTask(task)
    }
}

extension TasksPresenter: TasksInteractorOutputProtocol {

    func saveTaskDidSuccess() {
        router.popViewController(from: view?.navigationController)
    }

    func saveTaskDidFail() {

    }

    func retrieveTasksDidSuccess(retrievedTasks: [StoredTask]) {
        view?.reloadTable(tasks: retrievedTasks)
    }

    func retrieveTasksDidFail() {

    }

    func updateTaskDidSuccess() {

    }

    func updateTaskDidFail() {

    }

    func deleteTaskDidSuccess(deletedTask task: StoredTask) {
        view?.removeTask(task: task)
    }

    func deleteTaskDidFail() {

    }
}
