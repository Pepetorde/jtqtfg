//
//  TasksInteractor.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 25/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import CoreData

class TasksInteractor: TasksInteractorProtocol {

    var presenter: TasksInteractorOutputProtocol?
    private let dataSource: TasksDataSourceProtocol

    init(dataSource: TasksDataSourceProtocol) {
        self.dataSource = dataSource
    }
}

extension TasksInteractor {
    func saveTask(newTask: Task) {
        dataSource.saveTask(newTask: newTask)
    }

    func retrieveTasks() {
        dataSource.retrieveTasks()
    }

    func updateTask(_ task: StoredTask, with changes: Task) {

        dataSource.updateTask(task, with: changes)
    }

    func deleteTask(_ task: StoredTask) {
        dataSource.deleteTask(task)
    }
}

extension TasksInteractor: TasksDataSourceOutputProtocol {

    func saveTaskDidSuccess() {
        presenter?.saveTaskDidSuccess()
    }

    func saveTaskDidFail() {
        presenter?.saveTaskDidFail()
    }

    func retrieveTasksDidSuccess(retrievedTasks: [StoredTask]) {
        presenter?.retrieveTasksDidSuccess(retrievedTasks: retrievedTasks)
    }

    func retrieveTasksDidFail() {
        presenter?.retrieveTasksDidFail()
    }

    func updateTaskDidSuccess() {
        presenter?.updateTaskDidSuccess()
    }

    func updateTaskDidFail() {
        presenter?.updateTaskDidFail()
    }

    func deleteTaskDidSuccess(deletedTask task: StoredTask) {
        presenter?.deleteTaskDidSuccess(deletedTask: task)
    }

    func deleteTaskDidFail() {
        presenter?.deleteTaskDidFail()
    }
}
