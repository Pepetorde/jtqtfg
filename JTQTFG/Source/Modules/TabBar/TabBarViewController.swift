//
//  TabBarViewController.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    var presenter: TabBarPresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension TabBarViewController: TabBarViewControllerProtocol {

}
