//
//  TabBarProtocols.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation
import UIKit

protocol TabBarRouterProtocol: class {

}

protocol TabBarPresenterProtocol: class {

}

protocol NestedTabBarViewRouterProtocol: class {
    var tabIcon: UIImage { get }
    var tabTitle: String { get }

    func getAnidatedTabBarModule() -> UIViewController
}

protocol TabBarViewControllerProtocol: class {

}
