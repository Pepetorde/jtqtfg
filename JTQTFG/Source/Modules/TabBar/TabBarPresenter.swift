//
//  TabBarPresenter.swift
//  TFG
//
//  Created by José Tordesillas Quintana on 16/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation

class TabBarPresenter: TabBarPresenterProtocol {

    weak private var view: TabBarViewControllerProtocol?
    private let router: TabBarRouterProtocol

    init(view: TabBarViewControllerProtocol, router: TabBarRouterProtocol) {
        self.view = view
        self.router = router
    }
}
