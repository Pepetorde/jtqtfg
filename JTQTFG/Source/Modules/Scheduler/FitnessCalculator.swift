//
//  FitnessCalculator.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 22/06/2020.
//  Copyright © 2020 José Tordesillas Quintana. All rights reserved.
//

import Foundation

struct FitnessSettings {
    let minimumTimePoints = Double(15)
    let completionPoints = Double(10)
    let overlappingPoints = Double(15)
    let optionalDaysPoints = Double(10)
    let homogeneityPoints = Double(10)

    let availableTimePoints = Double(30)
    let taskWindowPoints = Double(30)
}

class FitnessCalculator {
    let tasks: [Task]
    let workDays: [WorkDay]
    var workWindowForTaskWithIndex: [(startDate: Int, deadline: Int)] = []

    let settings = FitnessSettings()

    var individual = Individual()

    var minimumTimeInfo = MinimumTimeInfo()
    var completionInfo: CompletionInfo
    var optionalDaysInfo = OptionalDaysInfo()
    var homogeneityInfo = HomogeneityInfo()
    var availableTimeInfo = AvailableTimeInfo()
    var taskWindowInfo = TaskWindowInfo()

    init(tasks: [Task], workDays: [WorkDay]) {
        self.tasks = tasks
        self.workDays = workDays
        self.completionInfo = CompletionInfo(tasks: tasks, workDays: workDays)
        initTasksWorkWindow()
    }

    private func initTasksWorkWindow() {
        workWindowForTaskWithIndex = []
        for taskIndex in 0..<tasks.count {
            let startDateIndex = workDays.first?.date.daysAwayFrom(tasks[taskIndex].startDate) ?? 0
            let endDateIndex = workDays.first?.date.daysAwayFrom(tasks[taskIndex].deadline) ?? 0
            workWindowForTaskWithIndex.append((startDateIndex, endDateIndex))
        }
    }

    func calculatePopulationFitness(population: [Individual]) -> [Double] {
        var populationFitness = [Double]()
        for individual in population {
            resetStatistics()
            populationFitness.append(fitnessOf(individual: individual))
        }
        return populationFitness
    }

    private func fitnessOf(individual: Individual) -> Double {
        self.individual = individual
        runOnceThroughIndividual()

  //      log(individual: individual)
        var fitness = Double(0)
        fitness += minimumTimePoints()
        fitness += completionPoints()
        fitness += overlappingPoints()
        fitness += optionalDaysPoints()
        fitness += homogeneityPoints()
        fitness *= availableTimeDivisor()
        fitness *= taskWindowDivisor()

  //      print("Fitness: \(fitness)")
        return fitness
    }

    private func resetStatistics() {
        minimumTimeInfo.reset()
        completionInfo.reset()
        optionalDaysInfo.reset()
        homogeneityInfo.reset()
        availableTimeInfo.reset()
        taskWindowInfo.reset()
        //...
    }

    private func runOnceThroughIndividual() {
        for slotIndex in 0..<individual.count {
            gatherInfoAboutIndividual(onIndex: slotIndex)
        }
    }

    private func gatherInfoAboutIndividual(onIndex slotIndex: Int) {
        updateMinimumTimeInfo(onIndex: slotIndex)
        updateCompletionInfo(onIndex: slotIndex)
        updateOptionalDaysInfo(onIndex: slotIndex)
        updateHomogeneityAndAvailableTimeInfo(onIndex: slotIndex)
        updateTaskWindowInfo(onIndex: slotIndex)
    }

    private func updateMinimumTimeInfo(onIndex slotIndex: Int) {
        let taskIndex = taskIndexFromSlot(withIndex: slotIndex)
        let task = tasks[taskIndex]

        if individual[slotIndex] == 0 {
            minimumTimeInfo.slotsThatComplyWithMinimumTime += 1
        } else if Double(individual[slotIndex]) >= task.minimumTime {
            minimumTimeInfo.slotsThatComplyWithMinimumTime += 1
        }
    }

    private func taskIndexFromSlot(withIndex slotIndex: Int) -> Int {
        return slotIndex % tasks.count
    }

    private func updateCompletionInfo(onIndex slotIndex: Int) {
        let taskIndex = taskIndexFromSlot(withIndex: slotIndex)
        let dayIndex = dayIndexFromSlot(withIndex: slotIndex)

        if individual[slotIndex] > 0 {
            if completionInfo.firstDayWithWorkOfTask[taskIndex] == nil {
                completionInfo.firstDayWithWorkOfTask[taskIndex] = dayIndex
            }
            completionInfo.lastDayWithWorkOfTask[taskIndex] = dayIndex
        }
    }

    private func dayIndexFromSlot(withIndex slotIndex: Int) -> Int {
        let taskIndex = taskIndexFromSlot(withIndex: slotIndex)
        return (slotIndex - taskIndex) / tasks.count
    }

    private func updateOptionalDaysInfo(onIndex slotIndex: Int) {
        let dayIndex = dayIndexFromSlot(withIndex: slotIndex)
        let workDay = workDays[dayIndex]
        if workDay.optional == true {
            optionalDaysInfo.numberOfOptionalDaySlots += 1
            if individual[slotIndex] == 0 {
                optionalDaysInfo.slotsComplyingWithOptionalDays += 1
            }
        }
    }

    private func updateHomogeneityAndAvailableTimeInfo(onIndex slotIndex: Int) {
        updateCurrentDayWorkTime(onIndex: slotIndex)

        if isLastSlotOfDay(slotIndex: slotIndex) {
            updateLeastAndBusiestDaysWorkTime()
            updateDaysComplyingWithAvailableTime(onIndex: slotIndex)
            homogeneityInfo.currentDayWorkTime = 0
        }
    }

    private func updateCurrentDayWorkTime(onIndex slotIndex: Int) {
        if individual[slotIndex] > 0 {
            let taskIndex = taskIndexFromSlot(withIndex: slotIndex)
            homogeneityInfo.currentDayWorkTime += individual[slotIndex] + Int(tasks[taskIndex].contextChange)
        }
    }

    private func isLastSlotOfDay(slotIndex: Int) -> Bool {
        return (slotIndex + 1) % tasks.count == 0
    }

    private func updateLeastAndBusiestDaysWorkTime() {
        if homogeneityInfo.currentDayWorkTime > homogeneityInfo.busiestDayWorkTime {
            homogeneityInfo.busiestDayWorkTime = homogeneityInfo.currentDayWorkTime
        }
        if homogeneityInfo.currentDayWorkTime < homogeneityInfo.leastBusyDayWorkTime {
            homogeneityInfo.leastBusyDayWorkTime = homogeneityInfo.currentDayWorkTime
        }
    }

    private func updateDaysComplyingWithAvailableTime(onIndex slotIndex: Int) {
        let dayIndex = dayIndexFromSlot(withIndex: slotIndex)
        let workDay = workDays[dayIndex]
        if workDay.availableTime >= Double(homogeneityInfo.currentDayWorkTime) {
            availableTimeInfo.daysComplyingWithAvailableTime += 1
        }
    }

    private func updateTaskWindowInfo(onIndex slotIndex: Int) {
        let taskIndex = taskIndexFromSlot(withIndex: slotIndex)
        let taskWindow = workWindowForTaskWithIndex[taskIndex]
        let dayIndex = dayIndexFromSlot(withIndex: slotIndex)

        if (taskWindow.startDate...taskWindow.deadline).contains(dayIndex) == false {
            taskWindowInfo.outOfWindowSlots += 1
            if individual[slotIndex] == 0 {
                taskWindowInfo.slotsComplyingWithTaskWindow += 1
            }
        }
    }
}

extension FitnessCalculator {
    private func minimumTimePoints() -> Double {
        let totalNumberOfSlots = Double(individual.count)
        let factor = Double(minimumTimeInfo.slotsThatComplyWithMinimumTime)/totalNumberOfSlots
        return factor * settings.minimumTimePoints
    }

    private func completionPoints() -> Double {
        var sumOfFactors = Double(0)
        var numberOfFactors = 0
        for taskIndex in 0..<tasks.count {
            if tasks[taskIndex].completion == .asap {
                numberOfFactors += 1
                sumOfFactors += completionFactorOfAsapTask(withIndex: taskIndex)
            }
            if tasks[taskIndex].completion == .alap {
                numberOfFactors += 1
                sumOfFactors += completionFactorOfAlapTask(withIndex: taskIndex)
            }
        }
        if numberOfFactors == 0 {
            return settings.completionPoints
        } else {
            let averageOfFactors = sumOfFactors / Double(numberOfFactors)
            return averageOfFactors * settings.completionPoints
        }
    }

    private func completionFactorOfAsapTask(withIndex taskIndex: Int) -> Double {
        let startDate = completionInfo.workWindowForTaskWithIndex[taskIndex].startDate
        let lastDayWithWork = completionInfo.lastDayWithWorkOfTask[taskIndex]!
        let lastScenarioDate = dayIndexFromSlot(withIndex: individual.count - 1)

        let window = lastScenarioDate - startDate + 1

        if lastDayWithWork < startDate || window == 1 {
            return 1
        } else {
            return Double(lastScenarioDate-lastDayWithWork)/Double(lastScenarioDate-startDate)
        }
    }

    private func completionFactorOfAlapTask(withIndex taskIndex: Int) -> Double {
        let deadline = completionInfo.workWindowForTaskWithIndex[taskIndex].deadline
        let firstDayWithWork = completionInfo.firstDayWithWorkOfTask[taskIndex]!

        if deadline == 0 || firstDayWithWork > deadline {
            return 1
        } else {
            return Double(firstDayWithWork) / Double(deadline)
        }
    }

    private func overlappingPoints() -> Double {
        var sumOfFactors = Double(0)
        var numberOfFactors = 0
        for taskIndex in 0..<tasks.count where tasks[taskIndex].overlap == false {
            numberOfFactors += 1
            sumOfFactors += overlappingFactorForTask(withIndex: taskIndex)
        }

        if numberOfFactors == 0 {
            return settings.overlappingPoints
        } else {
            let averageOfFactors = sumOfFactors / Double(numberOfFactors)
            return averageOfFactors * settings.overlappingPoints
        }
    }

    private func overlappingFactorForTask(withIndex taskIndex: Int) -> Double {
        let lowerNonOverlappingBoundDay = completionInfo.firstDayWithWorkOfTask[taskIndex]! + 1
        let upperNonOverlappingBoundDay = completionInfo.lastDayWithWorkOfTask[taskIndex]! - 1
        let nonOverlappingWindow = upperNonOverlappingBoundDay - lowerNonOverlappingBoundDay + 1

        if nonOverlappingWindow < 1 {
            return 1
        } else {
            let slotsThatComplyWithOverlapping = slotsThatComplyWithOverlappingForTask(withIndex: taskIndex)
            let numberOfSlotsOfOtherTasks = nonOverlappingWindow * (tasks.count - 1)
            return Double(slotsThatComplyWithOverlapping) / Double(numberOfSlotsOfOtherTasks)
        }
    }

    private func slotsThatComplyWithOverlappingForTask(withIndex taskIndex: Int) -> Int {
        var slotsThatComplyWithOverlapping = 0
        for slotIndex in nonOverlappingSlotWindowForTask(withIndex: taskIndex)
            where slotCompliesWithOverlappingForTask(withIndex: taskIndex, slotIndex: slotIndex) {
                slotsThatComplyWithOverlapping += 1
        }
        return slotsThatComplyWithOverlapping
    }

    private func nonOverlappingSlotWindowForTask(withIndex taskIndex: Int) -> ClosedRange<Int> {
        let lowerNonOverlappingBoundDay = completionInfo.firstDayWithWorkOfTask[taskIndex]! + 1
        let upperNonOverlappingBoundDay = completionInfo.lastDayWithWorkOfTask[taskIndex]! - 1

        let firstSlotIndexOfFirstDay = lowerNonOverlappingBoundDay * tasks.count
        let lastSlotIndexOfLastDay = (upperNonOverlappingBoundDay + 1) * tasks.count - 1

        return firstSlotIndexOfFirstDay...lastSlotIndexOfLastDay
    }

    private func slotCompliesWithOverlappingForTask(withIndex taskIndex: Int, slotIndex: Int) -> Bool {
        let taskIndexOfSlot = taskIndexFromSlot(withIndex: slotIndex)
        if taskIndexOfSlot != taskIndex && individual[slotIndex] == 0 {
            return true
        } else {
            return false
        }
    }

    private func optionalDaysPoints() -> Double {
        if optionalDaysInfo.numberOfOptionalDaySlots == 0 {
            return settings.optionalDaysPoints
        } else {
            let slotsThatComply = Double(optionalDaysInfo.slotsComplyingWithOptionalDays)
            let totalOptionalDaySlots = Double(optionalDaysInfo.numberOfOptionalDaySlots)
            let factor = slotsThatComply / totalOptionalDaySlots
            return factor * settings.optionalDaysPoints
        }
    }

    private func homogeneityPoints() -> Double {
        let factor = calculateFactorForHomogeneity()
        return factor * settings.homogeneityPoints
    }

    private func calculateFactorForHomogeneity() -> Double {
        let totalWorkTime = tasks.reduce(0, {result, task in result + task.workTime + task.contextChange})
        let busiestWorkTime = homogeneityInfo.busiestDayWorkTime
        let leastBusyWorkTime = homogeneityInfo.leastBusyDayWorkTime
        let factor = 1 - (Double(busiestWorkTime-leastBusyWorkTime)) / Double(totalWorkTime)
        return factor
    }

    private func availableTimeDivisor() -> Double {
        let factor = Double(availableTimeInfo.daysComplyingWithAvailableTime) / Double(workDays.count)
        let points = settings.availableTimePoints
        let divisor = 1 / (points - factor * (points - 1))

        return divisor
    }

    private func taskWindowDivisor() -> Double {
        if taskWindowInfo.outOfWindowSlots == 0 {
            return 1
        } else {
            let factor = Double(taskWindowInfo.slotsComplyingWithTaskWindow) / Double(taskWindowInfo.outOfWindowSlots)
            let points = settings.taskWindowPoints
            let divisor = 1 / (points - factor * (points - 1))

            return divisor
        }
    }
}

extension FitnessCalculator {

    internal func log(individual: Individual) {
        print()
        print()

        var firstRow = "  "
        var secondRow = "  "
        for task in tasks {
            firstRow.append(task.name + " ")
            secondRow.append("__")
        }
        print(firstRow)
        print(secondRow)

        for dayIndex in 0..<workDays.count {

            var line = ""
            if dayIndex + 1 < 10 {
                line = String(dayIndex + 1) + " |"
            } else {
                line = String(dayIndex + 1) + "|"
            }
            for taskIndex in 0..<self.tasks.count {
                let slotIndex = dayIndex * tasks.count + taskIndex
                let taskTime = individual[slotIndex]
                if taskTime >= 10 {
                    line.append(String(taskTime))
                } else if taskTime == 0 {
                    line.append("  ")
                } else {
                    line.append(String(taskTime) + " ")
                }
            }
            print(line)
            print(" " + "|")
        }
    }
}
