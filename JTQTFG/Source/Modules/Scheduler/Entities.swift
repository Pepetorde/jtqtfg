//
//  Entities.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 09/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation

struct Scenario {
    var taskList: [Task]
    var workDays: [WorkDay]
}

struct WorkCalendar {
    var workDays: [WorkDay]
    var doNotFit: [NotFittingTask]

    var notEnoughTime: Bool {
        if doNotFit.isEmpty {
            return false
        } else {
            return true
        }
    }

    var busiestWorkDay: WorkDay? {
        return workDays.max {workDay1, workDay2 in workDay1.totalWorkTime < workDay2.totalWorkTime}
    }

    var lastWorkDay: WorkDay? {
        return workDays.max {workDay1, workDay2 in workDay1.date < workDay2.date}
    }

    init(workDays: [WorkDay], doNotFit: [NotFittingTask]) {
        self.workDays = workDays
        self.doNotFit = doNotFit
    }

    init() {
        workDays = []
        doNotFit = []
    }

    init(from calendar: StoredWorkCalendar) {
        self.workDays = []
        let storedWorkDays = calendar.workDays?.allObjects as? [StoredWorkDay] ?? []
        for storedWorkDay in storedWorkDays {
            let workDay = WorkDay(from: storedWorkDay)
            self.workDays.append(workDay)
        }

        self.doNotFit = []
        let storedNonFittingTasks = calendar.doNotFit?.allObjects as? [StoredNotFittingTask] ?? []
        for storedDoNonFittingTask in storedNonFittingTasks {
            let notFittingTask = NotFittingTask(from: storedDoNonFittingTask)
            self.doNotFit.append(notFittingTask)
        }
    }
}

struct WorkDay: Equatable, Comparable {
    var date: Date
    var availableTime: TimeInterval
    var work: [WorkFragment]
    var optional: Bool

    var totalWorkTime: TimeInterval {
        var total: TimeInterval = 0
        for workFragment in work where workFragment.workTime > 0 {
            total += workFragment.workTime
            total += workFragment.task.contextChange
        }
        return total
    }

    init(date: Date, availableTime: TimeInterval, optional: Bool) {
        self.date = date
        self.availableTime = availableTime
        self.work = []
        self.optional = optional
    }

    init(from storedWorkDay: StoredWorkDay) {
        self.date = storedWorkDay.date!
        self.availableTime = storedWorkDay.availableTime

        self.work = []
        let storedWork = storedWorkDay.work?.allObjects as? [StoredWorkFragment] ?? []
        for storedWorkFragment in storedWork {
            let workFragment = WorkFragment(from: storedWorkFragment)
            self.work.append(workFragment)
        }

        self.optional = storedWorkDay.optional
    }

    static func == (lhs: WorkDay, rhs: WorkDay) -> Bool {
        if lhs.date == rhs.date {
            return true
        } else {
            return false
        }
    }

    static func < (lhs: WorkDay, rhs: WorkDay) -> Bool {
        return lhs.date < rhs.date
    }
}

struct WorkFragment {
    var task: Task
    var workTime: TimeInterval
    var done: Bool

    init(task: Task, workTime: TimeInterval, done: Bool) {
        self.task = task
        self.workTime = workTime
        self.done = done
    }

    init(from storedWorkFragment: StoredWorkFragment) {
        self.task = Task(from: storedWorkFragment.task!)
        self.workTime = storedWorkFragment.workTime
        self.done = storedWorkFragment.done
    }

    init() {
        self.task = Task()
        self.workTime = 0
        self.done = false
    }

    static func == (lhs: WorkFragment, rhs: WorkFragment) -> Bool {
        if lhs.task == rhs.task && lhs.workTime == rhs.workTime {
            return true
        } else {
            return false
        }
    }
}

struct NotFittingTask {
    var task: Task
    var howMuch: TimeInterval

    init(from storedNotFittingTask: StoredNotFittingTask) {
        self.task = Task(from: storedNotFittingTask.task!)
        self.howMuch = storedNotFittingTask.howMuch
    }

    init(task: Task, howMuch: TimeInterval) {
        self.task = task
        self.howMuch = howMuch
    }
}
