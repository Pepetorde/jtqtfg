//
//  GeneticAlgorithm.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 22/06/2020.
//  Copyright © 2020 José Tordesillas Quintana. All rights reserved.
//

import Foundation

class GeneticAlgorithm {

    var tasks: [Task]
    var workDays: [WorkDay]
    let workTimeSlot: TimeInterval

    var population = Population()
    var newPopulation = Population()

    var populationFitness = [Double]()
    var bestFitness = Double(0)
    var bestIndiviual = Individual()

    let populationSize = 100
    let tournamentSize = 2

    let mutationProbability = Double(0.01)
    let maxSearchTime = TimeInterval(60)
    let maxGensWithoutImprovement = 250

    let fitnessCalculator: FitnessCalculator

    init(tasks: [Task], workDays: [WorkDay], workTimeSlot: TimeInterval) {
        self.tasks = tasks
        self.workDays = workDays
        self.workTimeSlot = workTimeSlot

        fitnessCalculator = FitnessCalculator(tasks: tasks, workDays: workDays)
    }

    func findCalendarWithGenetics() -> WorkCalendar {
        let populationGenerator = PopulationGenerator(populationSize: populationSize, tasks: tasks, workDays: workDays)
        population = populationGenerator.generateInitialPopulation()

        let startTime = Date()
        var elapsedTime = TimeInterval(0)

        var gensWithoutChange = 0
        var previousBestFitness = Double(0)

        while elapsedTime < maxSearchTime && gensWithoutChange < maxGensWithoutImprovement /* true */ {
            generation()

            elapsedTime = Date().timeIntervalSince(startTime)
            if previousBestFitness != bestFitness {
                gensWithoutChange = 0
            } else {
                gensWithoutChange += 1
            }
            previousBestFitness = bestFitness
        }

        return createCalendar(from: bestIndiviual)
    }

    internal func generation() {
        populationFitness = fitnessCalculator.calculatePopulationFitness(population: population)
        saveBestIndividual()

        for _ in 1...populationSize {
            let parents = selection()
            let child = crossover(parents: parents)
            let mutatedChild = mutate(individual: child)
            newPopulation.append(mutatedChild)
        }
        population = newPopulation
        newPopulation = []
    }

    private func saveBestIndividual() {
        for individualIndex in 0..<populationSize where populationFitness[individualIndex] > bestFitness {
            bestIndiviual = population[individualIndex]
            bestFitness = populationFitness[individualIndex]

            log(individual: bestIndiviual)
            print("Fitness: " + String(bestFitness))
        }
    }

    private func selection() -> (Individual, Individual) {
        let firstParent = tournamentSelection()
        let secondParent = tournamentSelection()
        return (firstParent, secondParent)
    }

    private func tournamentSelection() -> Individual {
        let tournamentMembers = selectMembersForTournament()

        let fittest = tournamentMembers.max { (individual1, individual2) -> Bool in
            return populationFitness[individual1.individualIndex] < populationFitness[individual2.individualIndex]
        }
        return fittest!.individual
    }

    private func selectMembersForTournament() -> [(individual: Individual, individualIndex: Int)] {
        var tournamentMembers = [(individual: Individual, individualIndex: Int)]()
        for _ in 1...tournamentSize {
            let individualIndex = Int.random(in: 0..<populationSize)
            tournamentMembers.append((population[individualIndex], individualIndex))
        }
        return tournamentMembers
    }

    private func crossover(parents: (Individual, Individual)) -> Individual {
        let mask = createMask()
        var child = Individual()
        for slotIndex in 0..<mask.count {
            if mask[slotIndex] == true {
                child.append(parents.0[slotIndex])
            } else {
                child.append(parents.1[slotIndex])
            }
        }
        normalizeTaskTime(ofIndividual: &child)
        return child
    }

    private func createMask() -> [Bool] {
        var mask = [Bool]()
        let numberOfSlots = tasks.count * workDays.count
        for _ in 1...numberOfSlots {
            mask.append(Bool.random())
        }
        return mask
    }

    private func normalizeTaskTime(ofIndividual individual: inout Individual) {
        for taskIndex in 0..<tasks.count {
            normalizeTimeOfTask(withIndex: taskIndex, on: &individual)
        }
    }

    private func normalizeTimeOfTask(withIndex taskIndex: Int, on individual: inout Individual) {
        let assignedTaskTimeSum = assignedTimeOfTask(withIndex: taskIndex, on: individual)

        let normalizingRatio = Double(tasks[taskIndex].workTime) / Double(assignedTaskTimeSum)

        var remainingTaskTime = Int(tasks[taskIndex].workTime)

        for dayIndex in 0..<workDays.count - 1 {
            let slotIndex = dayIndex * tasks.count + taskIndex
            var normalizedSlotValue = 0

            if assignedTaskTimeSum == 0 {
                normalizedSlotValue = Int(Double(tasks[taskIndex].workTime) / Double(workDays.count))
            } else {
                normalizedSlotValue = Int((Double(individual[slotIndex]) * normalizingRatio).rounded())
            }

            if normalizedSlotValue > remainingTaskTime {
                normalizedSlotValue = remainingTaskTime
            }

            individual[slotIndex] = normalizedSlotValue
            remainingTaskTime -= normalizedSlotValue
        }

        let lastTaskDaySlotIndex = (workDays.count - 1) * tasks.count + taskIndex
        individual[lastTaskDaySlotIndex] = remainingTaskTime
    }

    private func assignedTimeOfTask(withIndex taskIndex: Int, on individual: Individual) -> Int {
        var assignedTaskTimeSum = 0
        for dayIndex in 0..<workDays.count {
            let slotIndex = dayIndex * tasks.count + taskIndex
            assignedTaskTimeSum += individual[slotIndex]
        }
        return assignedTaskTimeSum
    }

    private func mutate(individual: Individual) -> Individual {
        var mutatedIndividual = individual

        for slotIndex in 0..<mutatedIndividual.count {
            let random = Double.random(in: 0...1)
            if random <= mutationProbability {
                let taskIndex = taskIndexFromSlot(withIndex: slotIndex)
                let task = tasks[taskIndex]
                mutatedIndividual[slotIndex] = Int.random(in: 0...Int(task.workTime))
            }
        }
        normalizeTaskTime(ofIndividual: &mutatedIndividual)

        return mutatedIndividual
    }

    internal func createCalendar(from individual: Individual) -> WorkCalendar {
        unreduceTasks()
        unreduceWorkDays()

        for slotIndex in 0..<individual.count {
            let task = tasks[taskIndexFromSlot(withIndex: slotIndex)]
            let dayIndex = dayIndexFromSlot(withIndex: slotIndex)

            if individual[slotIndex] > 0 {
                let workTime = Double(individual[slotIndex]) * workTimeSlot

                let workFragment = WorkFragment(task: task, workTime: workTime, done: false)
                workDays[dayIndex].work.append(workFragment)
            }
        }

        var calendar = WorkCalendar()
        for workDay in workDays {
            calendar.workDays.append(workDay)
        }
        return calendar
    }

    private func unreduceTasks() {
        let reducedTasks = tasks
        tasks = []
        for task in reducedTasks {
            var unreducedTask = task
            unreducedTask.workTime *= workTimeSlot
            unreducedTask.contextChange *= workTimeSlot
            unreducedTask.minimumTime *= workTimeSlot
            tasks.append(unreducedTask)
        }
    }

    private func unreduceWorkDays() {
        let reducedWorkDays = workDays
        workDays = []
        for workDay in reducedWorkDays {
            var unreducedWorkDay = workDay
            unreducedWorkDay.availableTime *= workTimeSlot
            workDays.append(unreducedWorkDay)
        }
    }

    private func dayIndexFromSlot(withIndex slotIndex: Int) -> Int {
        let taskIndex = taskIndexFromSlot(withIndex: slotIndex)
        return (slotIndex - taskIndex) / tasks.count
    }

    private func taskIndexFromSlot(withIndex slotIndex: Int) -> Int {
        return slotIndex % tasks.count
    }
}

extension GeneticAlgorithm {
    internal func log(individual: Individual) {

        if individual.isEmpty {
            return
        }

        print()
        print()

        var firstRow = "   "
        var secondRow = "   "
        for task in tasks {
            firstRow.append(task.name + " ")
            secondRow.append("__")
        }
        print(firstRow)
        print(secondRow)

        for dayIndex in 0..<workDays.count {

            var line = ""
            if dayIndex + 1 < 10 {
                line = String(dayIndex + 1) + " |"
            } else {
                line = String(dayIndex + 1) + "|"
            }
            for taskIndex in 0..<self.tasks.count {
                let slotIndex = dayIndex * tasks.count + taskIndex
                let taskTime = individual[slotIndex]
                if taskTime >= 10 {
                    line.append(String(taskTime))
                } else if taskTime == 0 {
                    line.append("  ")
                } else {
                    line.append(String(taskTime) + " ")
                }
            }
            print(line)
            print("  " + "|")
        }
    }

    private func log(population: [Individual]) {
        for individual in population {
            log(individual: individual)
        }
    }
}
