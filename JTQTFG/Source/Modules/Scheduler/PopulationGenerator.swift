//
//  PopulationGenerator.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 22/06/2020.
//  Copyright © 2020 José Tordesillas Quintana. All rights reserved.
//

import Foundation

class PopulationGenerator {

    let populationSize: Int
    let tasks: [Task]
    let workDays: [WorkDay]

    init(populationSize: Int, tasks: [Task], workDays: [WorkDay]) {
        self.populationSize = populationSize
        self.tasks = tasks
        self.workDays = workDays
    }

    func generateInitialPopulation() -> [Individual] {
        var population = [Individual]()
        for _ in 1...populationSize {
            let individual = randomIndividual()
            population.append(individual)
        }
        return population
    }

    private func randomIndividual() -> Individual {
        var individual = Individual()
        let lenghtOfIndividualArray = tasks.count * workDays.count

        for _ in 0..<lenghtOfIndividualArray {
            individual.append(Int.random(in: 0...100))
        }

        normalizeTaskTime(ofIndividual: &individual)

        return individual
    }

    private func normalizeTaskTime(ofIndividual individual: inout Individual) {
        for taskIndex in 0..<tasks.count {
            normalizeTimeOfTask(withIndex: taskIndex, on: &individual)
        }
    }

    private func normalizeTimeOfTask(withIndex taskIndex: Int, on individual: inout Individual) {
        let assignedTaskTimeSum = assignedTimeOfTask(withIndex: taskIndex, on: individual)

        let normalizingRatio = Double(tasks[taskIndex].workTime) / Double(assignedTaskTimeSum)

        var remainingTaskTime = Int(tasks[taskIndex].workTime)

        for dayIndex in 0..<workDays.count - 1 {
            let slotIndex = dayIndex * tasks.count + taskIndex
            var normalizedSlotValue = 0

            if assignedTaskTimeSum == 0 {
                normalizedSlotValue = Int(Double(tasks[taskIndex].workTime) / Double(workDays.count))
            } else {
                normalizedSlotValue = Int((Double(individual[slotIndex]) * normalizingRatio).rounded())
            }

            if normalizedSlotValue > remainingTaskTime {
                normalizedSlotValue = remainingTaskTime
            }

            individual[slotIndex] = normalizedSlotValue
            remainingTaskTime -= normalizedSlotValue
        }

        let lastTaskDaySlotIndex = (workDays.count - 1) * tasks.count + taskIndex
        individual[lastTaskDaySlotIndex] = remainingTaskTime
    }

    private func assignedTimeOfTask(withIndex taskIndex: Int, on individual: Individual) -> Int {
        var assignedTaskTimeSum = 0
        for dayIndex in 0..<workDays.count {
            let slotIndex = dayIndex * tasks.count + taskIndex
            assignedTaskTimeSum += individual[slotIndex]
        }
        return assignedTaskTimeSum
    }
}
