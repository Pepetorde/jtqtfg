//
//  IndividualAnalytics.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 25/06/2020.
//  Copyright © 2020 José Tordesillas Quintana. All rights reserved.
//

import Foundation

struct MinimumTimeInfo {
    var slotsThatComplyWithMinimumTime = 0

    mutating func reset() {
        slotsThatComplyWithMinimumTime = 0
    }
}

struct CompletionInfo {
    var firstDayWithWorkOfTask: [Int?]
    var lastDayWithWorkOfTask: [Int?]
    var workWindowForTaskWithIndex: [(startDate: Int, deadline: Int)] = []

    init(tasks: [Task], workDays: [WorkDay]) {
        firstDayWithWorkOfTask = [Int?](repeating: nil, count: tasks.count)
        lastDayWithWorkOfTask = [Int?](repeating: nil, count: tasks.count)
        initTasksWorkWindow(tasks: tasks, workDays: workDays)
    }

    private mutating func initTasksWorkWindow(tasks: [Task], workDays: [WorkDay]) {
        workWindowForTaskWithIndex = []
        for taskIndex in 0..<tasks.count {
            let startDateIndex = workDays.first?.date.daysAwayFrom(tasks[taskIndex].startDate) ?? 0
            let endDateIndex = workDays.first?.date.daysAwayFrom(tasks[taskIndex].deadline) ?? 0
            workWindowForTaskWithIndex.append((startDateIndex, endDateIndex))
        }
    }

    mutating func reset() {
        firstDayWithWorkOfTask = [Int?](repeating: nil, count: firstDayWithWorkOfTask.count)
        lastDayWithWorkOfTask = [Int?](repeating: nil, count: lastDayWithWorkOfTask.count)
    }
}

struct OptionalDaysInfo {
    var slotsComplyingWithOptionalDays = 0
    var numberOfOptionalDaySlots = 0

    mutating func reset() {
        slotsComplyingWithOptionalDays = 0
        numberOfOptionalDaySlots = 0
    }
}

struct HomogeneityInfo {
    var leastBusyDayWorkTime = Int.max
    var busiestDayWorkTime = 0
    var currentDayWorkTime = 0

    mutating func reset() {
        leastBusyDayWorkTime = Int.max
        busiestDayWorkTime = 0
        currentDayWorkTime = 0
    }
}

struct AvailableTimeInfo {
    var daysComplyingWithAvailableTime = 0

    mutating func reset() {
        daysComplyingWithAvailableTime = 0
    }
}

struct TaskWindowInfo {
    var outOfWindowSlots = 0
    var slotsComplyingWithTaskWindow = 0

    mutating func reset() {
        outOfWindowSlots = 0
        slotsComplyingWithTaskWindow = 0
    }
}
