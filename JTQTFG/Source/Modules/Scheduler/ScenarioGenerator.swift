//
//  ScenarioGenerator.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 09/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation

class ScenarioGenerator {

    var firstWorkDayDate: Date
    var tasks: [Task]
    var schedule: StoredSchedule
    var exceptions: [StoredException]

    init(date: Date, tasks: [StoredTask], schedule: StoredSchedule, exceptions: [StoredException]) {
        self.firstWorkDayDate = date.startOfDay
        self.tasks = []
        for task in tasks where task.workTime > 0 {
            self.tasks.append(Task(from: task))
        }
        self.schedule = schedule
        self.exceptions = exceptions
    }

    func generateScenario() -> Scenario {
        let workDays = generateWorkDays()
        let scenario = Scenario(taskList: self.tasks, workDays: workDays)
        return scenario
    }

    private func generateWorkDays() -> [WorkDay] {
        var workDays = [WorkDay]()

        let lastWorkDay = getLastWorkDayDate()
        let dateComponents = DateComponents(hour: 0, minute: 1)
        Calendar.current.enumerateDates(startingAfter: self.firstWorkDayDate,
                                        matching: dateComponents,
                                        matchingPolicy: .nextTimePreservingSmallerComponents) { (date, _, stop) in
            if let date = date {
                if date > lastWorkDay {
                    stop = true
                } else {
                    let workDay = generateWorkDay(from: date.startOfDay)
                    workDays.append(workDay)
                }
            }
        }
        return workDays
    }

    private func getLastWorkDayDate() -> Date {
        let maximumDeadline = tasks.max(by: {$0.deadline <= $1.deadline})?.deadline.addingTimeInterval(60)
        return maximumDeadline ?? Date()
    }

    private func generateWorkDay(from workDayDate: Date) -> WorkDay {
        let availableTimeAndOptional = getAvailableTimeAndOptional(for: workDayDate)
        let workDay = WorkDay(date: workDayDate, availableTime: availableTimeAndOptional.availableTime,
                              optional: availableTimeAndOptional.optional)
        return workDay
    }

    private func getAvailableTimeAndOptional(for workDayDate: Date) -> (availableTime: TimeInterval, optional: Bool) {
        var availableTimeAndOptional: (availableTime: TimeInterval, optional: Bool)

        availableTimeAndOptional.availableTime = 0
        availableTimeAndOptional.optional = false

        if let exception = self.exceptions.first(where: {$0.date == workDayDate}) {
            availableTimeAndOptional.availableTime = exception.workTime
            availableTimeAndOptional.optional = exception.optional
        } else {
            availableTimeAndOptional = getAvailableTimeAndOptionalFromSchedule(for: workDayDate)
        }

        return availableTimeAndOptional
    }

    private func getAvailableTimeAndOptionalFromSchedule(for workDayDate: Date) ->
        (availableTime: TimeInterval, optional: Bool) {

        let weekDay = Calendar.current.component(.weekday, from: workDayDate)
        switch weekDay {
        case 1:
            return (schedule.sundayTime, schedule.sundayOptional)
        case 2:
            return (schedule.mondayTime, schedule.mondayOptional)
        case 3:
            return (schedule.tuesdayTime, schedule.tuesdayOptional)
        case 4:
            return (schedule.wednesdayTime, schedule.wednesdayOptional)
        case 5:
            return (schedule.thursdayTime, schedule.thursdayOptional)
        case 6:
            return (schedule.fridayTime, schedule.fridayOptional)
        default:
            return (schedule.saturdayTime, schedule.saturdayOptional)
        }
    }
}
