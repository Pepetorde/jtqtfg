//
//  Scheduler.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 09/07/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import Foundation

typealias Individual = [Int]
typealias Population = [Individual]

class Scheduler {
    let workTimeSlot = TimeInterval(300)

    var tasks: [Task] = []
    var workDays: [WorkDay] = []

    var auxTasks: [Task] = []
    var auxWorkDays: [WorkDay] = []

    init(scenario: Scenario) {
        initTasks(scenarioTasks: scenario.taskList)
        initWorkDays(scenarioWorkDays: scenario.workDays)
    }
}

extension Scheduler {
    private func initTasks(scenarioTasks: [Task]) {
        self.tasks = reduceTasks(scenarioTasks: scenarioTasks)
        self.tasks = self.tasks.sorted {task1, task2 in task1.deadline < task2.deadline}
    }

    private func reduceTasks(scenarioTasks: [Task]) -> [Task] {
        var reducedTasks = [Task]()
        for task in scenarioTasks {
            var reducedTask = task
            reducedTask.workTime /= workTimeSlot
            reducedTask.contextChange /= workTimeSlot
            reducedTask.minimumTime /= workTimeSlot
            reducedTasks.append(reducedTask)
        }
        return reducedTasks
    }

    private func initWorkDays(scenarioWorkDays: [WorkDay]) {
        self.workDays = reduceWorkDays(scenarioWorkDays: scenarioWorkDays)
        self.workDays = self.workDays.sorted {day1, day2 in day1.date < day2.date}
    }

    private func reduceWorkDays(scenarioWorkDays: [WorkDay]) -> [WorkDay] {
        var reducedWorkDays = [WorkDay]()
        for workDay in scenarioWorkDays {
            var reducedWorkDay = workDay
            reducedWorkDay.availableTime /= workTimeSlot
            reducedWorkDays.append(reducedWorkDay)
        }
        return reducedWorkDays
    }
}

extension Scheduler {
    func generateCalendar() -> WorkCalendar {

        if tasks.count == 0 || workDays.count == 0 {
            return WorkCalendar()
        }

        if scenarioHasSolution() {
            let geneticAlgorithm = GeneticAlgorithm(tasks: tasks, workDays: workDays, workTimeSlot: workTimeSlot)
            return geneticAlgorithm.findCalendarWithGenetics()
        } else {
            tasks = tasks.filter {task in task.optional == false}
            if tasks.count == 0 || workDays.count == 0 {
                return WorkCalendar()
            }

            if scenarioHasSolution() {
                let geneticAlgorithm = GeneticAlgorithm(tasks: tasks, workDays: workDays, workTimeSlot: workTimeSlot)
                return geneticAlgorithm.findCalendarWithGenetics()
            } else {
                return createCalendarWithNotFittingTasks()
            }
        }
    }

    private func scenarioHasSolution() -> Bool {
        self.auxTasks = self.tasks
        self.auxWorkDays = self.workDays

        for workDayIndex in 0..<auxWorkDays.count {
            fillWorkDay(withIndex: workDayIndex)
        }

        return auxTasks.allSatisfy({task in task.workTime == 0})
    }

    private func fillWorkDay(withIndex workDayIndex: Int) {
        for taskIndex in 0..<auxTasks.count {
            let task = auxTasks[taskIndex]
            let workDay = auxWorkDays[workDayIndex]
            if (task.startDate...task.deadline).contains(workDay.date) {
                tryToDealTask(withIndex: taskIndex, intoWorkDayWithIndex: workDayIndex)
            }
        }
    }

    private func tryToDealTask(withIndex taskIndex: Int, intoWorkDayWithIndex workDayIndex: Int) {
        if workDayhasFreeTime(workDayIndex: workDayIndex) {
            let timeToBeDealt = timeToBeDealtFromTask(withIndex: taskIndex, intoWorkDayWithIndex: workDayIndex)
            if timeToBeDealt > 0 {
                dealTask(withIndex: taskIndex, intoWorkDayWithIndex: workDayIndex, amount: timeToBeDealt)
            }
        }
    }

    private func workDayhasFreeTime(workDayIndex: Int) -> Bool {
        let freeWorkTime = auxWorkDays[workDayIndex].availableTime - auxWorkDays[workDayIndex].totalWorkTime
        if freeWorkTime > 0 {
            return true
        } else {
            return false
        }
    }

    private func timeToBeDealtFromTask(withIndex taskIndex: Int,
                                       intoWorkDayWithIndex workDayIndex: Int) -> TimeInterval {
        let task = auxTasks[taskIndex]
        let freeWorkTime = auxWorkDays[workDayIndex].availableTime - auxWorkDays[workDayIndex].totalWorkTime

        if task.workTime + task.contextChange <= freeWorkTime {
            return task.workTime
        } else if freeWorkTime > task.contextChange {
            return freeWorkTime - task.contextChange
        } else {
            return 0
        }
    }

    private func dealTask(withIndex taskIndex: Int, intoWorkDayWithIndex workDayIndex: Int, amount: TimeInterval) {
        let workFragment = WorkFragment(task: auxTasks[taskIndex], workTime: amount, done: false)
        auxWorkDays[workDayIndex].work.append(workFragment)
        auxTasks[taskIndex].workTime -= amount
    }

    private func createCalendarWithNotFittingTasks() -> WorkCalendar {
        var workCalendar = WorkCalendar()
        for task in auxTasks where task.workTime > 0 {
            let notFittingTask = NotFittingTask(task: task, howMuch: task.workTime * workTimeSlot)
            workCalendar.doNotFit.append(notFittingTask)
        }
        return workCalendar
    }
}
