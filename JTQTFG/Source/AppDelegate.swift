//
//  AppDelegate.swift
//  JTQTFG
//
//  Created by José Tordesillas Quintana on 18/06/2019.
//  Copyright © 2019 José Tordesillas Quintana. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var mainRouter: MainRouter?

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        if application.isFirstLaunch() {
            createInitialDatabase()
            UserDefaults.standard.set(true, forKey: "scenarioHasChanged")
            UserDefaults.standard.set(true, forKey: "dayChangeHasChanged")
            UserDefaults.standard.set(true, forKey: "popoverModeIsAmount")
            UserDefaults.standard.set(Date(), forKey: "lastSchedulingDate")
        }

        let window = UIWindow(frame: UIScreen.main.bounds)
        let mainRouter = MainRouter(mainWindow: window)
        window.makeKeyAndVisible()

        self.window = window
        self.mainRouter = mainRouter

        mainRouter.presentTabBarController()

        return true
    }

    private func createInitialDatabase() {
        createDefaultSchedule()
        createDefaultDayChange()
    }

    private func createDefaultSchedule() {
        let context = persistentContainer.viewContext
        _ = StoredSchedule(entity: StoredSchedule.entity(), insertInto: context)

        do {
            try persistentContainer.viewContext.save()
        } catch let error as NSError {
            print("No ha sido posible guardar \(error), \(error.userInfo)")
        }
    }

    private func createDefaultDayChange() {
        let context = persistentContainer.viewContext
        _ = StoredDayChange(entity: StoredDayChange.entity(), insertInto: context)

        do {
            try persistentContainer.viewContext.save()
        } catch let error as NSError {
            print("No ha sido posible guardar \(error), \(error.userInfo)")
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {

    }

    func applicationDidEnterBackground(_ application: UIApplication) {

    }

    func applicationWillEnterForeground(_ application: UIApplication) {

    }

    func applicationDidBecomeActive(_ application: UIApplication) {

    }

    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "JTQTFG")
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
